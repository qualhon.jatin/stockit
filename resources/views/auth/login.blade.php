@extends('dashboard.authBase')

@section('content')
<style>
  .card.p-4{
    background-color:#fbce07;
  }
  .btn-primary{
    background-color:#dd1d21;
    border-color:#dd1d21;
  }
</style>
<div class="login-outer-p">
           <div class="log-wrap">
				
              <figure>
                <img src="{{url('assets/img/Shell_logo.png')}}">
              </figure>
           <div class="login-form-outer">
		   <h1>Welcome to the eRoute2market
Online Stock Holding and Ordering System</h1>
              @error('msg')
                  <h4 class="alert alert-danger login-err-msg">{{ $message }}</h4>
              @enderror
            <form method="POST" action="{{ route('login') }}">
                  @csrf
				  <div class="form-group">
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><span class="icon-user"></span></span>
  <input class="form-control" type="text" placeholder="Enter username" name="user_name" value="{{ old('user_name') }}"  aria-describedby="basic-addon1" autofocus>
</div>
</div>
              <!-- <div class="input-group mb-3 login-input">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <svg class="c-icon">
                      <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-user"></use>
                    </svg>
                  </span>
                </div>
                <input class="form-control" type="text" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}"  autofocus>
              </div> -->
                @error('user_name')
                      <div class="alert alert-danger login-err-msg">{{ $message }}</div>
                @enderror
				<div class="form-group">
<div class="input-group">
  <span class="input-group-addon" id="basic-addon2"><span class="icon-lock"></span></span>
  <input class="form-control" type="password" placeholder="{{ __('Password') }}" name="password" value="{{ old('password') }}" aria-describedby="basic-addon2">
</div>
</div>
             <!--  <div class="input-group mb-4 login-input">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <svg class="c-icon">
                      <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-lock-locked"></use>
                    </svg>
                  </span>
                </div>
                <input class="form-control" type="password" placeholder="{{ __('Password') }}" name="password" value="{{ old('password') }}"><br>
              </div> -->
              @error('password')
                      <div class="alert alert-danger login-err-msg">{{ $message }}</div>
                @enderror
                    <button class="btn btn-primary-custom logform-btn" type="submit">{{ __('Login') }}</button> 
               
            </form>
         
     </div>
   </div>

</div>

@endsection

@section('javascript')

@endsection
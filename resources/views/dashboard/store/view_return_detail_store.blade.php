@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>    

<div class="container-fluid">
    <div class="fade-in">
        <!-- /.row-->


        @if($return_products_info)
        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>        
                        <h4>Return # {{$return_info['return_number']}}
                            <a class="btn btn-primary-custom" href="{{url('create_return_store')}}" style="float:right;">Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                      <div class="row" style="margin-bottom:30px;">
                        <div class="col-lg-6"> 
                            <address>
                                <strong>Returned by:</strong><br>
                                {{$store_info["Name"]}}<br>
                                <span id="deliveryAddress">
                                    {{$store_info['Address']}}<br>
                                </span>
                                <span id = "telephone1">Tel #: {{ $store_info['Phone']}}
                                </span>
                            </address>
                            <span>
                                <strong>Site id : </strong>
                                {{$store_info['Site_ID']}}
                            </span><br>
                            <span>
                                <strong>distributor id : </strong>
                                {{$distributor_info['ScSellerID']}}
                            </span>
                        </div>
                        <div class="col-lg-6" style="text-align:right"> 
                            <address id="returnedTo">
                                <strong>Returned to:</strong><br>
                                {{$distributor_info['Name']}}<br>
                                {{$distributor_info['address']}}<br>
                                {{$distributor_info['Telephone']}}
                            </address>
                        </div>  
                      </div>  
                      <div class="row" style="margin-bottom:30px;text-align:right;">
                        <div class="col-lg-12"> 
                            <address id = "returnDate">
                                <strong>Return date:</strong><br>
                                <?php
                                        $return_date_aux = date_create('now');
                                        $return_date = date_modify($return_date_aux, "+2 hours")->format('j F Y H:i:s');
                                ?>
                                <?php print $return_date; ?><br><br>
                            </address>
                        </div>
                      </div>  
                      <table class="table table-responsive-sm">
                      <form class="form-horizontal" action="{{url('/submit_place_return_store')}}" method="post" >
                        {{csrf_field()}}
                        <thead>
                          <tr>
                            <th>Product Code</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>UOR</th>
                            <th>Reason</th>
                            <th>Notes</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($return_products_info as $k=>$product)
                            <tr>
                                <td>{{$product['ScProductCode']}}</td>
                                <td>{{$product['ScProductDescription']}}</td>
                                <td>{{$product['qty']}}</td>
                                <td>{{$product['UOM']}}</td>
                                <td>{{$product['Reason']}}</td>
                                <td>{{$product['Notes']}}</td>
                            </tr>
                         
                        @endforeach

                        </tbody>
                       
                        </form>
                      </table>
                      </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->
              @endif  


       
    </div>
</div>


@endsection

@section('javascript')

<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection
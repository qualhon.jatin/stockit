@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>    

<div class="container-fluid">
    <div class="fade-in">
        <!-- /.row-->


        @if($cart_items)
        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> <h4>Order # {{$order_number}}</h4></div>
                    <div class="card-body">
                      <div class="row" style="margin-bottom:30px;">
                        <div class="col-lg-6"> 
                            <address>
                                <strong>Ordered by:</strong><br>
                                {{$store_info["Name"]}}<br>

                                <span id="deliveryAddress">
                                    {{$store_info['Address']}}<br>
                                    
                                </span>
                                <span id = "telephone1">Tel #: {{ $store_info['Phone']}}
                                </span>
                            </address>
                            <span>
                                <strong>Site id : </strong>
                                {{$store_info['Site_ID']}}
                            </span><br>
                            <span>
                                <strong>distributor id : </strong>
                                {{$distributor_info['ScSellerID']}}
                            </span>
                        </div>
                        <div class="col-lg-6" style="text-align:right"> 
                            <address id="orderedTo">
                                <strong>Sent to:</strong><br>
                                {{$distributor_info['Name']}}<br>
                                {{$distributor_info['address']}}<br>
                                {{$distributor_info['Telephone']}}

                            </address>
                        </div>  
                      </div>  
                      <div class="row" style="margin-bottom:30px;text-align:right;">
                        <div class="col-lg-12"> 
                            <address id = "orderDate">
                                <strong>Order date:</strong><br>
                                <?php
                                        $order_date_aux = date_create('now');
                                        $order_date = date_modify($order_date_aux, "+2 hours")->format('j F Y H:i:s');
                                        $delivery_date = date('j F Y', strtotime($order_date. ' + 3 days'));
                                ?>
                                <?php print $order_date; ?><br><br>
                                <strong>Expected delivery date:</strong><br>
                                <?php print $expected_delivery_date; ?><br><br>
                            </address>
                        </div>
                      </div>  
                      <table class="table table-responsive-sm">
                        <thead>
                          <tr>
                            <th>Product code</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Excl. Price</th>
                            <th>VAT</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($cart_items as $cart_item)
                            <tr>
                                <td>{{$cart_item['product_info']['product_code_actual']}}</td>
                                <td>
                                    {{$cart_item['product_info']['ScProductDescription']}}
                                    @if($cart_item['back_order'] != "")
                                        <b>({{$cart_item['back_order']}})</b>
                                    @endif
                                </td>
                                <td>{{$cart_item['quantity']}}</td>
                                <td>{{$cart_item['product_info']['price']}}</td>
                                <td>{{$cart_item['product_info']['tax_factor']}}</td>
                                <td>{{$cart_item['product_info']['total_price']}}</td>
                            </tr>
                         
                        @endforeach

                        </tbody>
                      </table>
                      </div>
                      <div class="card-body">
                      <table class="table table-responsive-sm">
                        <tbody>      
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Excl.</b></td>
                                <td style="text-align:center">{{$total_amount_info['total_excl_amount']}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Tax</b></td>
                                <td colspan="2" style="text-align:center">{{$total_amount_info['total_tax_amount']}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Order</b></td>
                                <td  style="text-align:center">{{$total_amount_info['total_order_amount']}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Backorder</b></td>
                                <td  style="text-align:center">{{$total_amount_info['total_backorder_amount']}}</td>
                            </tr>
                            <tr>
                                <td colspan="6" style="text-align:right">
                                    <form action="{{url('/submit_place_order_store')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="distributor_id" value="{{$distributor_info['ID']}}">
                                        <input type="hidden" name="distributor_name" value="{{$distributor_info['Name']}}">
                                        <input type="hidden" name="distributor_order_number" value="">
                                        <input type="hidden" name="order_number" value="{{$order_number}}">
                                        <input type="hidden" id="sellerId" name="sellerId" value="1">
                                        <input type="hidden" id="userSellerId" name="userSellerId" value="1">
                                        <input type="hidden" id="telephone1" name="telephone1" value="{{ $store_info['Phone']}}">
                                        <input type="hidden" id="total_order_amount" name="total_order_amount" value="{{$total_amount_info['total_order_amount']}}">
                                        <input type="hidden" id="total_backorder_amount" name="total_backorder_amount" value="{{$total_amount_info['total_backorder_amount']}}">
                                        <input type="hidden" id="expected_delivery_date" name="expected_delivery_date" value="{{$expected_delivery_date}}">

                                        <button class="btn btn-success" type="submit" style="margin-right:20px;">Save and send order</button>
                                        <a style="float:right;" href="{{ URL::previous() }}">
                                            <button class="btn btn-danger" type="button">Cancel</button>
                                        </a> 
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                      </table>
                    
                    </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->
              @endif  


       
    </div>
</div>


@endsection

@section('javascript')

<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection
@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>

<div class="container-fluid">
    <div class="fade-in">
        <!-- /.row-->


        @if($backorder_products_info)
        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        <h4>Order # {{$backorder_info['back_order_number']}}
                            <a class="btn btn-primary-custom" href="{{url('backorder_history_store')}}" style="float:right;">Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                      <div class="row" style="margin-bottom:30px;">
                        <div class="col-lg-6">
                            <address>
                                <strong>Ordered by:</strong><br>
                                {{$store_info["Name"]}}<br>
                                <span id="deliveryAddress">
                                    {{$store_info['Address']}}<br>
                                </span>
                                <span id = "telephone1">Tel #: {{ $store_info['Phone']}}
                                </span>
                            </address>
                            <span>
                                <strong>Site id : </strong>
                                {{$store_info['Site_ID']}}
                            </span><br>
                            <span>
                                <strong>distributor id : </strong>
                                {{$distributor_info['ScSellerID']}}
                            </span>
                        </div>
                        <div class="col-lg-6" style="text-align:right">
                            <address id="orderedTo">
                                <strong>Sent to:</strong><br>
                                {{$distributor_info['Name']}}<br>
                                {{$distributor_info['address']}}<br>
                                {{$distributor_info['Telephone']}}

                            </address>
                        </div>
                      </div>
                      <div class="row" style="margin-bottom:30px;text-align:right;">
                        <div class="col-lg-12">
                            <address id = "orderDate">
                                <strong>Order date:</strong><br>
                                <?php
                                        $order_date_aux = date_create('now');
                                        $order_date = date_modify($order_date_aux, "+2 hours")->format('j F Y H:i:s');
                                        $delivery_date = date('j F Y', strtotime($order_date. ' + 3 days'));
                                ?>
                                <?php print $order_date; ?><br><br>
                                
                            </address>
                        </div>
                      </div>
                      <table class="table table-responsive-sm">
                        <thead>
                          <tr>
                            <th>Item</th>
                            <th>Excl. Price</th>
                            <th>VAT</th>
                            <th>Quantity</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $totalExcl = 0;
                            $totalVAT = 0;
                            $totalOrder = 0
                        ?>
                        @foreach ($backorder_products_info as $product)
                        <?php

                            $producttotal =  floatval ($product['qty']) * (floatval ($product['price']) + floatval ($product['tax']));
                            $totalVAT = $totalVAT + floatval ($product['tax']) * floatval ($product['qty']);
                            $totalExcl = $totalExcl + floatval ($product['qty']) * floatval ($product['price']);
                            $totalOrder = $totalOrder +  floatval ($product['qty']) * (floatval ($product['price']) + floatval ($product['tax']))
                        ?>
                            <tr>
                                <td>{{$product['ScProductDescription']}}</td>
                                <td>
                                    <?php echo 'R '.number_format($product['price'], 2, '.', ' ') ?>
                                </td>
                                <td>
                                    <?php echo 'R '.number_format(floatval ($product['tax']), 2, '.', ' ') ?>
                                </td>
                                <td>{{$product['qty']}}</td>
                                <td>
                                    <?php echo 'R '.number_format($producttotal, 2, '.', ' ') ?>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>
                      </table>
                      </div>
                      <div class="card-body">
                      <table class="table table-responsive-sm">
                      <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Excl.</b></td>
                                <td style="text-align:center"><?php echo 'R '.number_format(floatval ($totalExcl), 2, '.', ' ') ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Tax</b></td>
                                <td colspan="2" style="text-align:center"><?php echo 'R '.number_format(floatval ($totalVAT), 2, '.', ' ') ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Order</b></td>
                                <td  style="text-align:center"><?php echo 'R '.number_format(floatval ($totalOrder), 2, '.', ' ') ?></td>
                            </tr>
                        </tbody>
                      </table>

                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header">
                        <i class="fa fa-comment-o"></i>        
                        <h4>Back Order History</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Comment</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($back_order_history)
                                @foreach ($back_order_history as $history)
                                    <tr>
                                        <td>{{$history['date_added']}}</td>
                                        <td>{{$history['comment']}}</td>
                                        <td>{{$history['name']}}</td>
                                    </tr>
                                @endforeach
                            @else
                            <tr><td colspan="3" align="center" >No history found</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->
              @endif



    </div>
</div>


@endsection

@section('javascript')

<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection

@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>    

<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> <h4>Return History</h4></div>
                    <div class="card-body">
                      <table class="table table-responsive-sm">
                        <thead>
                          <tr>
                            <th>Return number</th>
                            <th>Distributor name</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($return_history_list)
                          @foreach ($return_history_list as $return_history)
                            <tr>
                                <td>{{$return_history['return_number']}}</td>
                                <td>{{$return_history['distributor_name']}}</td>
                                <td>{{$return_history['date']}}</td>
                                <td>{{$return_history['status']}}</td>
                                <td><a class="btn-primary-custom" href="{{url('/view_return_detail_store',['id'=>$return_history['return_id']])}}"><button class="btn btn-primary-custom">View Detail</button></a></td>
                            </tr>
                          @endforeach
                        @else
                            <tr>
                                <td colspan="5" style="text-align:center;">No record found</td>
                            </tr>
                        @endif  
                        </tbody>
                      </table>
                        {{ $return_history_data->links() }}
                    </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->
    </div>
</div>


@endsection

@section('javascript')


<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/sweetalert2.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection
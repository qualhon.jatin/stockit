@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>    

<div class="container-fluid">
    <div class="fade-in">
       
        
        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> <h4>Order History</h4></div>
                    <div class="card-body">
                      <table class="table table-responsive-sm">
                        <thead>
                          <tr>
                            <th>Order number</th>
                            <th>SG reference</th>
                            <th>Order Total</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($order_history_list)
                          @foreach ($order_history_list as $order_history)
                            <tr>
                                <td>{{$order_history['order_number']}}</td>
                                <td>{{$order_history['distributor_order_number']}}</td>
                                <td><?= "R ".number_format($order_history['total'], 2, '.', ' ') ?></td>
                                <td>{{$order_history['date']}}</td>
                                <td>{{$order_history['name']}}</td>
                                <td><a class="btn-primary-custom" href="{{url('/view_order_detail_store',['id'=>$order_history['order_id']])}}"><button class="btn btn-primary-custom">View Detail</button></a></td>
                            </tr>
                          @endforeach
                        @else
                            <tr>
                                <td colspan="6" style="text-align:center;">No record found</td>
                            </tr>
                        @endif
                        </tbody>
                      </table>
                        {{ $order_history_data->links() }}
                    </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->

    </div>
</div>


@endsection

@section('javascript')


<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/sweetalert2.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection
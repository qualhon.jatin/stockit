@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>    

<div class="container-fluid">
    <div class="fade-in">
        <!-- /.row-->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="create_store_order" action="{{url('/distributor_stock')}}" method="get" >
                    {{ csrf_field() }}  
                        <div class="card-header"><strong>Create Order</strong></div>
                            <div class="card-body">
                            @if ( Session::has('flash_message') )
                        <div class="alert {{ Session::get('flash_type') }}">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="distributor_id">Distributors</label>
                                        <select class="form-control" id="distributor_id" name="distributor_id" onchange="change_distributor()">
                                            <option value="">All</option>
                                            @foreach ($distributors as $distributor)
                                            <option value="{{$distributor['ID']}}" {{ $selected['distributor_id']== $distributor['ID'] ? 'selected' : ''  }} >{{$distributor['Name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('distributor_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <label for="shell_master_categ_id">Shell Master Category</label>
                                        <select class="form-control" id="shell_master_categ_id" name="shell_master_categ_id" onchange="change_master_categ(this.value,'')">
                                            <option value="">All</option>
                                            @foreach ($shell_master_categories as $shell_master_categ)
                                            <option value="{{$shell_master_categ['ID']}}" {{ $selected['shell_master_categ_id']== $shell_master_categ['ID'] ? 'selected' : ''  }} >{{$shell_master_categ['Name']}}</option>
                                            @endforeach
                                        </select>
                                        
                                        @error('shell_master_categ_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <label for="category_id">Categories</label>
                                        <select class="form-control" id="category_id" name="category_id" onchange="change_categ(this.value,'')">
                                            <option value="">All</option>
                                            @foreach ($categories as $category)
                                            <option value="{{$category['ID']}}" {{ $selected['category_id']== $category['ID'] ? 'selected' : ''  }} >{{$category['Name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('category_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <label for="manufacturer_id">Manufacturers</label>
                                        <select class="form-control" id="manufacturer_id" name="manufacturer_id" onchange="change_manufacturer(this.value,'')">
                                            <option value="">All</option>
                                            @foreach ($manufacturers as $manufacturer)
                                            <option value="{{$manufacturer['ID']}}" {{ $selected['manufacturer_id']== $manufacturer['ID'] ? 'selected' : ''  }} >{{$manufacturer['Name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('manufacturer_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="brand_id">Brands</label>
                                        <select class="form-control" id="brand_id" name="brand_id" onchange="change_brand(this.value,'','')">
                                            <option value="">All</option>
                                            @foreach ($brands as $brand)
                                            <option value="{{$brand['ID']}}" {{ $selected['brand_id']== $brand['ID'] ? 'selected' : ''  }} >{{$brand['Name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('brand_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <label for="pack_size_id">Pack sizes</label>
                                        <select class="form-control" id="pack_size_id" name="pack_size_id">
                                            <option value="">All</option>
                                            @foreach ($pack_sizes as $pack_size)
                                            <option value="{{$pack_size['pack_size_id']}}" {{ $selected['pack_size_id']== $pack_size['pack_size_id'] ? 'selected' : ''  }} >{{$pack_size['pack_size_name']}}</option>
                                            @endforeach
                                        </select>
                                        @error('pack_size_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-3">
                                        <label  for="product_description">Product Description</label>
                                        <input class="form-control" id="product_description" type="text" name="product_description" placeholder="Product description" value="{{$selected['ScProductDescription']}}">
                                        @error('product_description')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div> 
                                    <div class="col-md-3 justify-content-center">
                                        <input type="hidden" name="form_submit" value="1">
                                        <button class="btn btn-primary-custom search-button" type="submit"> Search</button>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="card-footer">
                               
                                <!--<button class="btn btn-primary-custom" type="submit"> Submit</button>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row-->

        @if($products)
        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> <h4>Product List</h4></div>
                    <div class="card-body">
                      <table class="table table-responsive-sm">
                        <thead>
                          <tr>
                            <th>Product code</th>
                            <th>Item</th>
                            <th>Excl. Price</th>
                            <th>VAT</th>
                            <th>Stock</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{$product['product_code_actual']}}</td>
                                <td>{{$product['ScProductDescription']}}</td>
                                <td>{{$product['price']}}</td>
                                <td>{{$product['tax_factor']}}</td>
                                <td>{{$product['qnty']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                      
                        {{ $product_lists_result->links() }}
                     
                    </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->
              @endif  
    </div>
</div>


@endsection

@section('javascript')


<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/sweetalert2.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection
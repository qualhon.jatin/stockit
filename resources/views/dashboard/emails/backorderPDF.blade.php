<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - #123</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #FBCE07;
            /* color: #FFF; */
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
		p.address_stit{
			word-break: break-word;
			width: 229px;
		}
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
				<img src="{{ url('/assets/img/shellsmall.png')}}" alt="Logo" width="65" class="logo"/>
                <!--<img src="{{ url('/assets/img/redstockit.png')}}" alt="Logo" width="200" class="logo"/>-->
                <h2>Order # {{$order_info['back_order_number']}}</h2>
                <address>
                    <strong>Ordered by:</strong><br>
                    {{$store_info["Name"]}}<br>
                    <span id="deliveryAddress">
                        {{$store_info['Address']}}<br>
                    </span>
                    <span id = "telephone1">Tel #: {{ $store_info['Phone']}}
                    </span>
                </address>
                <span>
                    <strong>Site id : </strong>
                    {{$store_info['Site_ID']}}
                </span><br>
                <span>
                    <strong>distributor id : </strong>
                    {{$distributor_info['ScSellerID']}}
                </span>

            </td>
            <td align="right" style="width: 40%;">
				<img src="{{ url('/assets/img/redstockit.png')}}" alt="Logo" width="200" class="logo"/>
                <!--<img src="{{ url('/assets/img/shellsmall.png')}}" alt="Logo" width="30" class="logo"/>-->
                <h3>Sent to:</h3>
                <address id="orderedTo">
                    {{$distributor_info['Name']}}<br>
                    <p class="address_stit">{{$distributor_info['address']}}</p><br>
                    {{$distributor_info['Telephone']}}
                </address>
                <br>
                <address id="orderedTo">
                    <strong>Order date:</strong><br>
                    <?php
                            $order_date_aux = date_create('now');
                            $order_date = date_modify($order_date_aux, "+2 hours")->format('j F Y H:i:s');
                            $delivery_date = date('j F Y', strtotime($order_date. ' + 3 days'));
                    ?>
                    <?php print $order_date; ?><br><br>
                </address>
            </td>
        </tr>
    </table>
</div>
<br/>
<div class="invoice">
    <h3>Invoice # {{$order_info['back_order_number']}}</h3>
    <table width="100%">
        <thead style="text-align:left">
            <tr>
                <th>Product code</th>
                <th>Item</th>
                <th>Excl. Price</th>
                <th>VAT</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $totalExcl = 0;
            $totalVAT = 0;
            $totalOrder = 0
        ?>
        @foreach ($order_products_info as $product)
        <?php
            $producttotal =  floatval ($product['qty']) * (floatval ($product['price']) + floatval ($product['tax']));
            $totalVAT = $totalVAT + floatval ($product['tax']) * floatval ($product['qty']);
            $totalExcl = $totalExcl + floatval ($product['qty']) * floatval ($product['price']);
            $totalOrder = $totalOrder +  floatval ($product['qty']) * (floatval ($product['price']) + floatval ($product['tax']));
        ?>
        <tr>
            <td>{{$product['SellerCode']}}</td>
            <td>{{$product['ScProductDescription']}}</td>
            <td>
                <?php echo 'R '.number_format($product['price'], 2, '.', ' ') ?>
            </td>
            <td>
                <?php echo 'R '.number_format(floatval ($product['tax']), 2, '.', ' ') ?>
            </td>
            <td>{{$product['qty']}}</td>
            <td>
                <?php echo 'R '.number_format($producttotal, 2, '.', ' ') ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
        @endforeach
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td align="left">Total Excl.</td>
            <td align="left" class="gray"><?php echo 'R '.number_format(floatval ($totalExcl), 2, '.', ' ') ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="left">Total Tax</td>
            <td align="left" class="gray"><?php echo 'R '.number_format(floatval ($totalVAT), 2, '.', ' ') ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="left">Total Tax</td>
            <td align="left" class="gray"><?php echo 'R '.number_format(floatval ($totalOrder), 2, '.', ' ') ?></td>
        </tr>
        </tfoot>
    </table>
</div>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
            @ {{ date('Y') }} eRoute2market
            </td>
            <td align="right" style="width: 50%;">
                Stock.IT
            </td>
        </tr>

    </table>
</div>
</body>
</html>
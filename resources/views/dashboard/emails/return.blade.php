<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - #123</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #FBCE07;
            /* color: #FFF; */
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <img src="{{ url('/assets/img/redstockit.png')}}" alt="Logo" width="200" class="logo"/>
                <h2>Return # {{$return_info['return_number']}}</h2>
                <address>
                    <strong>Returned by:</strong><br>
                    {{$store_info["Name"]}}<br>
                    <span id="deliveryAddress">
                        {{$store_info['Address']}}<br>
                    </span>
                    <span id = "telephone1">Tel #: {{ $store_info['Phone']}}
                    </span>
                </address>
                <span>
                    <strong>Site id : </strong>
                    {{$store_info['Site_ID']}}
                </span><br>
                <span>
                    <strong>distributor id : </strong>
                    {{$distributor_info['ScSellerID']}}
                </span>

            </td>
            <td align="right" style="width: 40%;">
                <img src="{{ url('/assets/img/shellsmall.png')}}" alt="Logo" width="30" class="logo"/>
                <h3>Sent to:</h3>
                <address id="returnedTo">
                    {{$distributor_info['Name']}}<br>
                    {{$distributor_info['address']}}<br>
                    {{$distributor_info['Telephone']}}
                </address>
                <br>
                <address id="returnedTo">
                    <strong>Return date:</strong><br>
                    <?php
                            $return_date_aux = date_create('now');
                            $return_date = date_modify($return_date_aux, "+2 hours")->format('j F Y H:i:s');
                    ?>
                    <?php print $return_date; ?><br><br>
                </address>
            </td>
        </tr>
    </table>
</div>
<br/>
<div class="invoice">
    <h3>Invoice # {{$return_info['return_number']}}</h3>
    <table width="100%">
        <thead style="text-align:left">
            <tr>
                <th>Product Code</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>UOR </th>
                <th>Reason</th>
                <th>Notes</th>
            </tr>
        </thead>
        <tbody>
       
        @foreach ($return_products_info as $product)
       
        <tr>
            <td>{{$product['SellerCode']}}</td>
            <td >{{$product['ScProductDescription']}}</td>
            <td>{{$product['qty']}}</td>
            <td>{{$product['UOM']}}</td>
            <td>{{$product['Reason']}}</td>
            <td>{{$product['Notes']}}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
        @endforeach
        
    </table>
</div>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
            @ {{ date('Y') }} eRoute2market
            </td>
            <td align="right" style="width: 50%;">
                Stock.IT
            </td>
        </tr>

    </table>
</div>
</body>
</html>

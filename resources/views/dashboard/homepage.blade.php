@extends('dashboard.base')

@section('content')

          <div class="container-fluid">
            <div class="fade-in">
            <div class="row">
              <div class="col-sm-6 col-lg-4">
                <div class="card dash-up-card dash-up-card1">
                  <div class="card-body p-3 d-flex align-items-center">
                   <!--<div class="bg-gradient-info p-3 mfe-3">
                  <svg class="c-icon c-icon-xl">
                    <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-basket"></use>
                  </svg>
                  </div> -->
                  <div>
                  <div class="text-value">{{  $orders }}</div>
                  <div class="text-muted text-uppercase font-weight-bold small dash-text-u">Orders</div>
				   <a class="btn-block dash-ach" href="{{url('order_history_store')}}">
                      
                       <span class="icon-down-arrow"></span>
                    </a>
                  </div>
				  <figure>
				 <img src="{{url('assets/img/graph1.png')}}">
				  </figure>
                  </div>
                  <!-- <div class="card-footer px-3 py-2">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{url('order_history_store')}}">
                      <span class="small font-weight-bold">View More</span>
                        <svg class="c-icon">
                        <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right"></use>
                      </svg>
                    </a>
                  </div> -->
                </div>  
              </div>
              <div class="col-sm-6 col-lg-4">
                <div class="card dash-up-card dash-up-card2">
                  <div class="card-body p-3 d-flex align-items-center">
                  <!--<div class="bg-gradient-success p-3 mfe-3">
                  <svg class="c-icon c-icon-xl">
                    <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-basket"></use>
                  </svg>
                  </div> -->
                  <div>
                  <div class="text-value text-success">{{ $total_back_orders }}</div>
                  <div class="text-muted text-uppercase font-weight-bold small dash-text-u">Back Orders</div>
				  <a class="btn-block dash-ach" href="{{url('backorder_history_store')}}">
                      <span class="icon-down-arrow"></span>
                    </a>
                  </div>
				   <figure>
				 <img src="{{url('assets/img/graph2.png')}}">
				  </figure>
                  </div>
                  <!-- <div class="card-footer px-3 py-2">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{url('backorder_history_store')}}">
                      <span class="small font-weight-bold">View More</span>
                        <svg class="c-icon">
                        <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right"></use>
                      </svg>
                    </a>
                  < -->
                </div>  
              </div>
              <div class="col-sm-6 col-lg-4">
                <div class="card dash-up-card dash-up-card3">
                  <div class="card-body p-3 d-flex align-items-center">
                  <!-- <div class="bg-gradient-warning p-3 mfe-3">
                  <svg class="c-icon c-icon-xl">
                    <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-basket"></use>
                  </svg>
                  </div> -->
                  <div>
                  <div class="text-value text-warning">{{ $total_orders }}</div>
                  <div class="text-muted text-uppercase font-weight-bold small dash-text-u">Total Orders</div>
				  <a class="btn-block dash-ach" href="#">
                      <span class="icon-down-arrow"></span>
                    </a>
                  </div>
				   <figure>
				 <img src="{{url('assets/img/graph3.png')}}">
				  </figure>
                  </div>
                  <!-- <div class="card-footer px-3 py-2">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="#">
                      <span class="small font-weight-bold">View More</span>
                        <svg class="c-icon">
                        <use xlink:href="assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right"></use>
                      </svg>
                    </a>
                  </div> -->
                </div>  
              </div>
            </div>
              <div class="card">
              <div class="card-header">
                <div class="row">
                <div class="col-sm-5">
                      <h4 class="card-title mb-0"> Sales Analytics</h4>
                    </div>
                    <div class="col-sm-5 d-none d-md-block">
                      <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
                        <label class="btn btn-outline-secondary active" onClick="getData('today')" >
                          <input type="radio" name="options" value="today" autocomplete="off" checked > Today
                        </label>
                        <label class="btn btn-outline-secondary" onClick="getData('week')" >
                          <input type="radio" name="options" value="week" autocomplete="off" > Week
                        </label>
                        <label class="btn btn-outline-secondary" onClick="getData('month')" >
                          <input type="radio" name="options" value="month" autocomplete="off"> Month
                        </label>
                        <label class="btn btn-outline-secondary" onClick="getData('year')" >
                          <input type="radio" name="options" value="year" autocomplete="off"> Year
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-2 d-none d-md-block">
                      <div class="form-check form-check-inline mr-1 cstm-checkbox">
					 
                          <label class="cstm--check form-check-label" for="order-checkbox">
							 <input class="form-check-input" id="order-checkbox" type="checkbox" value="check1" checked onChange="changeChart()" >
  							Order<span class="checkmark"></span>
						</label>
                    
                        <!--<input class="form-check-input" id="order-checkbox" type="checkbox" value="check1" checked onChange="changeChart()" >
                        <label class="form-check-label" for="order-checkbox"></label>-->
                      </div>
                      <div class="form-check form-check-inline mr-1 cstm-checkbox">
					   <label class="cstm--check form-check-label" for="back-order-checkbox">
							<input class="form-check-input" id="back-order-checkbox" type="checkbox" value="check2" checked onChange="changeChart()" >
  							Back Order<span class="checkmark"></span>
						</label>
                        <!-- <input class="form-check-input" id="back-order-checkbox" type="checkbox" value="check2" checked onChange="changeChart()" >
                        <label class="form-check-label" for="back-order-checkbox">Back Order</label> -->
                      </div>
                    </div>
                </div>
              </div>
                <div class="card-body">
                  <div class="c-chart-wrapper" style="height:300px;">
                    <canvas class="chart" id="main-chart" height="300"></canvas>
                  </div>
                </div>
              </div>
              <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-sm-2">
                    <h4 class="card-title mb-0"> Sales Comparison</h4>
                  </div>
                  <div class="col-sm-10 d-none d-md-block">
                    <?php for($i=date('Y');$i>date('Y', strtotime('-5 year'));$i--) { ?>
                      <div class="form-check form-check-inline mr-3 cstm-checkbox">
					  <label class="cstm--check form-check-label" for="order-checkbox{{$i}}">
							<input class="form-check-input compYears" id="order-checkbox{{$i}}" type="checkbox" name="check{{$i}}" value="{{$i}}" checked onChange="compChange()" />
  							{{ $i }}<span class="checkmark"></span>
						</label>
                        <!-- <input class="form-check-input compYears" id="order-checkbox{{$i}}" type="checkbox" name="check{{$i}}" value="{{$i}}" checked onChange="compChange()" />
                        <label class="form-check-label" for="order-checkbox{{$i}}">{{ $i }}</label> -->
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
                <div class="card-body">
                  <div class="c-chart-wrapper" style="height:300px;">
                    <canvas class="chart" id="canvas-1" height="300"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection

@section('javascript')
    <script src="{{ asset('js/jquery.min.js') }}" defer></script>
    <script src="{{ asset('js/Chart.min.js') }}"></script>
    <script src="{{ asset('js/coreui-chartjs.bundle.js') }}"></script>
    <script src="{{ asset('js/chart.js') }}" defer></script>
    <!-- <script src="{{ asset('js/main.js') }}" defer></script> -->
@endsection

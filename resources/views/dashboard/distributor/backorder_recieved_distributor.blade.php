@extends('dashboard.base')

@section('content')
<style>
.search-button {
	margin-top: 26px;
	padding: 7px 15px;
	font-weight: bolder;
}
label {
    font-weight: bolder;
}
</style>    

<div class="container-fluid">
    <div class="fade-in">

        <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i> <h4>Backorder History</h4></div>
                    <div class="card-body">
                      <table class="table table-responsive-sm">
                        <thead>
                          <tr>
                            <th>Order number</th>
                            <th>SG reference</th>
                            <th>Order Total</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($backorder_history_list)
                          @foreach ($backorder_history_list as $backorder_history)
                              <tr>
                                  <td>{{$backorder_history['back_order_number']}}</td>
                                  <td>{{$backorder_history['distributor_order_number']}}</td>
                                  <td><?= "R ".number_format($backorder_history['total'], 2, '.', ' ') ?></td>
                                  <td>{{$backorder_history['date']}}</td>
                                  <td>{{$backorder_history['name']}}</td>
                                  <td><a class="btn-primary-custom" href="{{url('/distributor/view_backorder_detail_distributor',['id'=>$backorder_history['back_order_id']])}}"><button class="btn btn-primary-custom">View Detail</button></a></td>
                              </tr>
                          @endforeach
                        @else
                        <tr><td colspan="6" align="center" >No record found</td></tr>
                        @endif
                        </tbody>
                      </table>
                        {{ $backorder_history_data->links() }}
                    </div>
                  </div>
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->  
    </div>
</div>


@endsection

@section('javascript')


<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/sweetalert2.min.js')}}"></script>
<script src="{{url('js/app.js')}}"></script>

@endsection
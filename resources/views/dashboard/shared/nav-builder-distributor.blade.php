<?php
/*
    $data = $menuel['elements']
*/

if(!function_exists('renderDropdown')){
    function renderDropdown($data){
        if(array_key_exists('slug', $data) && $data['slug'] === 'dropdown'){
            echo '<li class="c-sidebar-nav-dropdown">';
            echo '<a class="c-sidebar-nav-dropdown-toggle" href="#">';
            if($data['hasIcon'] === true && $data['iconType'] === 'coreui'){
                echo '<i class="' . $data['icon'] . ' c-sidebar-nav-icon"></i>';    
            }
            echo $data['name'] . '</a>';
            echo '<ul class="c-sidebar-nav-dropdown-items">';
            renderDropdown( $data['elements'] );
            echo '</ul></li>';
        }else{
            for($i = 0; $i < count($data); $i++){
                if( $data[$i]['slug'] === 'link' ){
                    echo '<li class="c-sidebar-nav-item">';
                    echo '<a class="c-sidebar-nav-link" href="' . $data[$i]['href'] . '">';
                    echo '<span class="c-sidebar-nav-icon"></span>' . $data[$i]['name'] . '</a></li>';
                }elseif( $data[$i]['slug'] === 'dropdown' ){
                    renderDropdown( $data[$i] );
                }
            }
        }
    }
}
?>

      <div class="c-sidebar-brand"><img class="c-sidebar-brand-full" src="{{ url('/assets/img/redstockit.png')}}" width="200px"  alt="CoreUI Logos"><img class="c-sidebar-brand-minimized" src="assets/brand/coreui-signet-white.svg" width="118" height="46" alt="CoreUI Logo"></div>
        <ul class="c-sidebar-nav">
        @if(isset($appMenus['sidebar menu']))
            @foreach($appMenus['sidebar menu'] as $menuel)
                @if($menuel['slug'] === 'link')
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{ $menuel['href'] }}">
                        @if($menuel['hasIcon'] === true)
                            @if($menuel['iconType'] === 'coreui')
                                <i class="{{ $menuel['icon'] }} c-sidebar-nav-icon"></i>
                            @endif
                        @endif 
                        {{ $menuel['name'] }}
                        </a>
                    </li>
                @elseif($menuel['slug'] === 'dropdown')
                    <?php renderDropdown($menuel) ?>
                @elseif($menuel['slug'] === 'title')
                    <li class="c-sidebar-nav-title">
                        @if($menuel['hasIcon'] === true)
                            @if($menuel['iconType'] === 'coreui')
                                <i class="{{ $menuel['icon'] }} c-sidebar-nav-icon"></i>
                            @endif
                        @endif 
                        {{ $menuel['name'] }}
                    </li>
                @endif
            @endforeach
        @endif
            <li class="c-sidebar-nav-item side-p distri-p">
                <a class="c-sidebar-nav-link" href="{{url('distributor/dashboard')}}">
                    <!--<svg class="c-icon">
                        <use xlink:href="{{ url('') }}/assets/icons/coreui/free-symbol-defs.svg#cui-speedometer"></use>
                    </svg> -->
					<span class="icon-data"></span>
                   <h2> Dashboard</h2>
                </a> 
            </li>
            
            <li class="c-sidebar-nav-item side-p distri-p">
                <a class="c-sidebar-nav-link" href="{{url('distributor/order_recieved_distributor')}}">
                    <!--<svg class="c-icon">
                        <use xlink:href="{{ url('') }}/assets/icons/coreui/free-symbol-defs.svg#cui-history"></use>
                    </svg>-->
					<span class="icon-checkout"></span>
                    <h2>Order Recieved</h2>
                </a>
            </li>
            <li class="c-sidebar-nav-item side-p distri-p">
                <a class="c-sidebar-nav-link" href="{{url('distributor/backorder_recieved_distributor')}}">
                    <!--<svg class="c-icon">
                        <use xlink:href="{{ url('') }}/assets/icons/coreui/free-symbol-defs.svg#cui-history"></use>
                    </svg>-->
					<span class="icon-return-1"></span>
                    <h2>Backorder Recieved</h2>
                </a>
            </li>
            <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('distributor/return_recieved_distributor')}}">
                   <span class="icon-return"></span>
                     <h2>Return Recieved</h2>
                </a>
            </li>
            <!--<li class="c-sidebar-nav-item side-p distri-p">
               
                <form action="{{url('logout')}}" method="POST">
                    @csrf
                    <button type="submit" class="btn  btn-block" style="color:#3c4b64;font-weight:501;padding:0px;">
                        <a  class="c-sidebar-nav-link" >
                            <svg class="c-icon">
                                <use xlink:href="{{ url('') }}/assets/icons/coreui/free-symbol-defs.svg#cui-account-logout"></use>
                            </svg>
							<span class="icon-logout-2"></span>
                            <h2>Logout</h2>
                        </a>
                    </button>  
                </form>    
            </li>-->
        </ul>
        <!--<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>-->
    </div>
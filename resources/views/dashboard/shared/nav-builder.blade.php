<?php
/*
    $data = $menuel['elements']
*/

if(!function_exists('renderDropdown')){
    function renderDropdown($data){
        if(array_key_exists('slug', $data) && $data['slug'] === 'dropdown'){
            echo '<li class="c-sidebar-nav-dropdown">';
            echo '<a class="c-sidebar-nav-dropdown-toggle" href="#">';
            if($data['hasIcon'] === true && $data['iconType'] === 'coreui'){
                echo '<i class="' . $data['icon'] . ' c-sidebar-nav-icon"></i>';    
            }
            echo $data['name'] . '</a>';
            echo '<ul class="c-sidebar-nav-dropdown-items">';
            renderDropdown( $data['elements'] );
            echo '</ul></li>';
        }else{
            for($i = 0; $i < count($data); $i++){
                if( $data[$i]['slug'] === 'link' ){
                    echo '<li class="c-sidebar-nav-item">';
                    echo '<a class="c-sidebar-nav-link" href="' . $data[$i]['href'] . '">';
                    echo '<span class="c-sidebar-nav-icon"></span>' . $data[$i]['name'] . '</a></li>';
                }elseif( $data[$i]['slug'] === 'dropdown' ){
                    renderDropdown( $data[$i] );
                }
            }
        }
    }
}
?>

      <div class="c-sidebar-brand"><img class="c-sidebar-brand-full" src="{{ url('/assets/img/redstockit.png')}}" width="200px"  alt="CoreUI Logos"><img class="c-sidebar-brand-minimized" src="assets/brand/coreui-signet-white.svg" width="118" height="46" alt="CoreUI Logo"></div>
        <ul class="c-sidebar-nav">
        @if(isset($appMenus['sidebar menu']))
            @foreach($appMenus['sidebar menu'] as $menuel)
                @if($menuel['slug'] === 'link')
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{ $menuel['href'] }}">
                        @if($menuel['hasIcon'] === true)
                            @if($menuel['iconType'] === 'coreui')
                                <i class="{{ $menuel['icon'] }} c-sidebar-nav-icon"></i>
                            @endif
                        @endif 
                        {{ $menuel['name'] }}
                        </a>
                    </li>
                @elseif($menuel['slug'] === 'dropdown')
                    <?php renderDropdown($menuel) ?>
                @elseif($menuel['slug'] === 'title')
                    <li class="c-sidebar-nav-title">
                        @if($menuel['hasIcon'] === true)
                            @if($menuel['iconType'] === 'coreui')
                                <i class="{{ $menuel['icon'] }} c-sidebar-nav-icon"></i>
                            @endif
                        @endif 
                        {{ $menuel['name'] }}
                    </li>
                @endif
            @endforeach
        @endif
            <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('dashboard')}}">
                    <span class="icon-data"></span>
                    <h2>Dashboard</h2>
                </a>
            </li>
			 <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('distributor_stock')}}">
                    <span class="icon-stock"></span>
                   <h2>distributor stock</h2>
                </a>
            </li>
            <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('create_store_order')}}">
                    <span class="icon-checkout"></span>
                   <h2>create order</h2>
                </a>
            </li>
            <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('order_history_store')}}">
                   <span class="icon-history"></span>
                    <h2>order history</h2>
                </a>
            </li>
			 <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('backorder_history_store')}}">
                    <span class="icon-return-1"></span>
                    <h2>Backorder history</h2>
                </a>
            </li>
			 <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('create_return_store')}}">
                    <span class="icon-box"></span>
                    <h2>create return</h2>
                </a>
            </li>
            <li class="c-sidebar-nav-item side-p">
                <a class="c-sidebar-nav-link" href="{{url('return_history_store')}}">
                   <span class="icon-return"></span>
                     <h2>return history</h2>
                </a>
            </li>
            <!--<li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{url('create_return_store')}}">
                    Create Return
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{url('return_history_store')}}">
                    Return History
                </a> 
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{url('reports_store')}}">
                    Reports
                </a>
            </li>-->
            <!--<li class="c-sidebar-nav-item">
                <form action="{{url('logout')}}" method="POST">
                    @csrf
                    <button type="submit" class="btn  btn-block" style="color:#3c4b64;font-weight:501;padding:0px;">
                        <a  class="c-sidebar-nav-link" >
                        <svg class="c-icon">
                            <use xlink:href="{{ url('') }}/assets/icons/coreui/free-symbol-defs.svg#cui-account-logout"></use>
                        </svg>
                            Logout
                        </a>
                    </button>  
                </form>    
            </li>-->
        </ul>
        <!--<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>-->
    </div>
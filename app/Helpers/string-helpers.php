<?php

if ( !function_exists('getStockByProDis') ) {
	function getStockByProDis($product_id,$distributor_id){
        $prd_quantity = DB::table('SellerStock')
                ->where('product_id', $product_id)
                ->where('seller_id', $distributor_id)
                ->get()->first();
        $prd_quantity = json_decode(json_encode($prd_quantity),true);
        if(!empty($prd_quantity)) {
            return (int)$prd_quantity['quantity'];
        }
        return 0;
	}
}

if ( !function_exists('setStockByProDis') ) {
    function setStockByProDis($product_id,$distributor_id,$quantity){
        DB::table('SellerStock')
            ->where('product_id', $product_id)
            ->where('seller_id', $distributor_id)
            ->update([
                'quantity' => $quantity
            ]);
    }
}

if ( !function_exists('getReturnUOR') ) {
    function getReturnUOR($uor_id){
        $uor_info = DB::table('units_of_return')
            ->where('id', $uor_id)
            ->get()->first();
          
        $uor_info = json_decode(json_encode($uor_info),true);   
        if(!empty($uor_info)){
            return $uor_info['unit_of_return_name'];
        }else{
            return 'N/A';
        } 
        
    }
}

if ( !function_exists('getReturnReason') ) {
    function getReturnReason($reason_id){
        $reason_info = DB::table('reasons')
            ->where('reason_id', $reason_id)
            ->get()->first();
          
        $reason_info = json_decode(json_encode($reason_info),true);   
        if(!empty($reason_info)){
            return $reason_info['return_reason'];
        }else{
            return 'N/A';
        } 
    }
}

?>
<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Distributors;
use DB;
use Illuminate\Pagination\Paginator;
use Cart;
use Session;
use Illuminate\Support\Facades\Hash;
use Mail;
use PDF;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    public function __construct(){

       // if($user_info['user_type'] != 0){
           // return redirect('login');
       // }

    }
    public function check_login(){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);
       // print_r($user_info);
       //die();
    }
    public function create_store_order(Request $request){

        if(empty($request->distributor_id) && isset($_GET['distributor_id'])){
            Session::flash('flash_message', 'Please select any distributor!');
            Session::flash('flash_type', 'alert-danger');
        }

       // return $this->check_login();
        $product_stock = 0;

        $distributor_id = "";
        if(!empty($request->distributor_id)){
            $distributor_id = $request->distributor_id;
        }

        $cart_items = [];

        $cart_products = Cart::getContent();
        $cart_products = json_decode(json_encode($cart_products),true);

        // echo "<pre>";
        //print_r($cart_products); die;
        
        foreach($cart_products as $cart_product){
            if(strpos($cart_product['id'],"_return")){
                continue;
            } elseif(strpos($cart_product['id'],"_bck")){
                $product_id = str_replace("_bck","",$cart_product['id']);
            } else {
                $product_id = $cart_product['id'];
            }

            $cart_product['back_order'] = "";
            $cart_product['product_stock'] = getStockByProDis($product_id,$request->distributor_id);
            $cart_product['selected_distributor'] = $request->distributor_id;

            //$product_id = $cart_product['id'];

            if(empty($distributor_id)){
                continue;
            }


            $product_info = DB::table('Product')->select('Product.*','Tax.tax_factor','SellerProduct.SellerCode')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->where('SellerProduct.SellerID',$distributor_id)->get()->first();

            // echo "<pre>";
            // print_r($product_info);
            // die();
            $product_info = json_decode(json_encode($product_info),true);

            $product_info['price'] = 'R '.  number_format($product_info['price'], 2, '.', ' ');
            $product_info['tax_factor'] = $product_info['tax_factor'] * 100 . '%';

            if($distributor_id > 0 && $distributor_id <= 7){
                $product_info['product_code_actual'] = $product_info['SellerCode'];
            }else{
                $product_info['product_code_actual'] = $product_info['SellerCode'];
            }

            $cart_product['product_info'] = $product_info;

            if(strpos($cart_product['id'],"_bck")){
                $cart_product['back_order'] = "backorder";
            }

            $cart_items[] = $cart_product;

            // if($cart_product['quantity'] <= $product_stock){
            //     $cart_items[] = $cart_product;
            // }else{
            //     $cart_items[] = $cart_product;

               // $cart_product['quantity'] = $cart_product['quantity'] - $cart_product['product_stock'];

                //$cart_product['back_order'] = "backorder";
               // $cart_items[] = $cart_product;
            // }



        }

        // echo "<pre>";
        // print_r($cart_items);
        // die("hello");

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);




        $distributors = DB::table('Seller')->select('Seller.*')->join('StoreDistributor', 'StoreDistributor.SellerID', '=', 'Seller.id')->join('User','User.buyer_id','=','StoreDistributor.Buyer_eR2mID')->where('User.user_id',$user_info['user_id'])->distinct()->get();
        $distributors =  json_decode(json_encode($distributors), true);

        $shell_master_categories = DB::table('ClientMasterCategory')->orderBy('Name')->get();
        $shell_master_categories =  json_decode(json_encode($shell_master_categories), true);

        $categories = DB::table('Category')->orderBy('Name')->get();
        $categories = json_decode(json_encode($categories),true);

        $manufacturers = DB::select('select * from Supplier');
        $manufacturers = json_decode(json_encode($manufacturers),true);

        $brands = DB::table('Brand')->orderBy('Name')->get();
        $brands = json_decode(json_encode($brands),true);

        $pack_sizes = DB::table('pack_sizes')->orderBY('pack_size_name')->get();
        $pack_sizes = json_decode(json_encode($pack_sizes),true);


        $products = [];
        $product_lists_result = [];


        $selected = array(
            'distributor_id' => '',
            'shell_master_categ_id' => '',
            'category_id' => '',
            'manufacturer_id' => '',
            'brand_id' => '',
            'pack_size_id' => '',
            'ScProductDescription' => ''
        );

        if(!empty($request->form_submit) && !empty($request->distributor_id)){
            $distributor_id = $request->distributor_id;
            $shell_master_categ_id = $request->shell_master_categ_id;
            $category_id = $request->category_id;
            $manufacturer_id = $request->manufacturer_id;
            $brand_id = $request->brand_id;
            $pack_size_id = $request->pack_size_id;
            $product_description = $request->product_description;

            $selected = array(
                'distributor_id' => $distributor_id,
                'shell_master_categ_id' => $shell_master_categ_id,
                'category_id' => $category_id,
                'manufacturer_id' => $manufacturer_id,
                'brand_id' => $brand_id,
                'pack_size_id' => $pack_size_id,
                'ScProductDescription' => $product_description
            );

            $product_lists = DB::table('Product')->select('Product.*','Tax.tax_factor','SellerProduct.SellerCode')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('SellerProduct.SellerID',$distributor_id);

            $product_lists = $product_lists->where('ScProductCode','!=','');
            if(!empty($shell_master_categ_id)){
               $product_lists = $product_lists->where('client_master_category_id',$shell_master_categ_id);
            }
            if(!empty($category_id)){
                $product_lists = $product_lists->where('category_id',$category_id);
            }
            if(!empty($manufacturer_id)){
                //$product_lists = $product_lists->where('seller_id',$manufacturer_id);
            }
            if(!empty($brand_id)){
                $product_lists = $product_lists->where('brand_id',$brand_id);
            }
            if(!empty($pack_size_id)){
                $product_lists = $product_lists->where('pack_size_id',$pack_size_id);
            }
            if(!empty($product_description)){
                $product_lists = $product_lists->where('ScProductDescription', 'like', '%' . $product_description . '%');
            }

            $product_lists = $product_lists->distinct()->orderBy('ScProductDescription')->Paginate(10);
            $product_lists_result   = $product_lists;
            $product_lists_result_tmp = json_decode(json_encode($product_lists),true);

            // echo "<pre>";
            // print_r($product_lists);
            // die();

            // echo "<pre>";
            // print_r($product_lists_result['data']);
            // die();

            $product_lists = $product_lists_result_tmp['data'];

            foreach($product_lists as $product_list){
                $product_list['price'] = 'R '.  number_format($product_list['price'], 2, '.', ' ');
                $product_list['tax_factor'] = $product_list['tax_factor'] * 100 . '%';

                if($distributor_id > 0 && $distributor_id <= 7){
                    $product_list['product_code_actual'] = $product_list['SellerCode'];
                }else{
                    $product_list['product_code_actual'] = $product_list['SellerCode'];
                }


                $product_list['qnty'] = getStockByProDis($product_list['ID'],$request->distributor_id);
                $products[] = $product_list;
            }

            // echo "<pre>";
            // print_r($product_lists);

            // echo "Count : " . count($product_lists);
            // die();

            //$products = $product_lists;
        }

        // echo "<pre>";
        // print_r($distributors);
        // die("hello");
        if(!empty($_GET)){
            if(!empty($request->distributor_id)){
                $product_lists_result = $product_lists_result->appends($_GET);
            }
            return view('dashboard.store.create_store_order',['distributors'=>$distributors,'shell_master_categories'=>$shell_master_categories,'categories'=>$categories,'manufacturers'=>$manufacturers,'brands'=>$brands,'pack_sizes'=>$pack_sizes,'products'=>$products,'cart_items'=>$cart_items,'selected'=>$selected,'product_lists_result'=>$product_lists_result]);
        }else{
            return view('dashboard.store.create_store_order',['distributors'=>$distributors,'shell_master_categories'=>$shell_master_categories,'categories'=>$categories,'manufacturers'=>$manufacturers,'brands'=>$brands,'pack_sizes'=>$pack_sizes,'products'=>$products,'cart_items'=>$cart_items,'selected'=>$selected,'product_lists_result'=>$product_lists_result]);
        }

    }

    public function distributor_stock(Request $request){

        if(empty($request->distributor_id) && isset($_GET['distributor_id'])){
            Session::flash('flash_message', 'Please select any distributor!');
            Session::flash('flash_type', 'alert-danger');
        }

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);
        
        $distributors = DB::table('Seller')->select('Seller.*')->join('StoreDistributor', 'StoreDistributor.SellerID', '=', 'Seller.id')->join('User','User.buyer_id','=','StoreDistributor.Buyer_eR2mID')->where('User.user_id',$user_info['user_id'])->distinct()->get();
        $distributors =  json_decode(json_encode($distributors), true);

        $shell_master_categories = DB::table('ClientMasterCategory')->orderBy('Name')->get();
        $shell_master_categories =  json_decode(json_encode($shell_master_categories), true);

        $categories = DB::table('Category')->orderBy('Name')->get();
        $categories = json_decode(json_encode($categories),true);

        $manufacturers = DB::select('select * from Supplier');
        $manufacturers = json_decode(json_encode($manufacturers),true);

        $brands = DB::table('Brand')->orderBy('Name')->get();
        $brands = json_decode(json_encode($brands),true);

        $pack_sizes = DB::table('pack_sizes')->orderBY('pack_size_name')->get();
        $pack_sizes = json_decode(json_encode($pack_sizes),true);

        $selected = array(
            'distributor_id' => '',
            'shell_master_categ_id' => '',
            'category_id' => '',
            'manufacturer_id' => '',
            'brand_id' => '',
            'pack_size_id' => '',
            'ScProductDescription' => ''
        );

        $products = [];
        $product_lists_result = [];

        if(!empty($request->form_submit) && !empty($request->distributor_id)){
            $distributor_id = $request->distributor_id;
            $shell_master_categ_id = $request->shell_master_categ_id;
            $category_id = $request->category_id;
            $manufacturer_id = $request->manufacturer_id;
            $brand_id = $request->brand_id;
            $pack_size_id = $request->pack_size_id;
            $product_description = $request->product_description;

            $selected = array(
                'distributor_id' => $distributor_id,
                'shell_master_categ_id' => $shell_master_categ_id,
                'category_id' => $category_id,
                'manufacturer_id' => $manufacturer_id,
                'brand_id' => $brand_id,
                'pack_size_id' => $pack_size_id,
                'ScProductDescription' => $product_description
            );

            $product_lists = DB::table('Product')->select('Product.*','Tax.tax_factor','SellerProduct.SellerCode')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('SellerProduct.SellerID',$distributor_id);

            $product_lists = $product_lists->where('ScProductCode','!=','');
            if(!empty($shell_master_categ_id)){
               $product_lists = $product_lists->where('client_master_category_id',$shell_master_categ_id);
            }
            if(!empty($category_id)){
                $product_lists = $product_lists->where('category_id',$category_id);
            }
            if(!empty($manufacturer_id)){
                //$product_lists = $product_lists->where('seller_id',$manufacturer_id);
            }
            if(!empty($brand_id)){
                $product_lists = $product_lists->where('brand_id',$brand_id);
            }
            if(!empty($pack_size_id)){
                $product_lists = $product_lists->where('pack_size_id',$pack_size_id);
            }
            if(!empty($product_description)){
                $product_lists = $product_lists->where('ScProductDescription', 'like', '%' . $product_description . '%');
            }

            $product_lists = $product_lists->Paginate(10);
            $product_lists_result   = $product_lists;
            $product_lists_result_tmp = json_decode(json_encode($product_lists),true);


            $product_lists = $product_lists_result_tmp['data'];

            foreach($product_lists as $product_list){
                $product_list['price'] = 'R '.  number_format($product_list['price'], 2, '.', ' ');
                $product_list['tax_factor'] = $product_list['tax_factor'] * 100 . '%';

                if($distributor_id > 0 && $distributor_id <= 7){
                    $product_list['product_code_actual'] = $product_list['SellerCode'];
                }else{
                    $product_list['product_code_actual'] = $product_list['SellerCode'];
                }


                $product_list['qnty'] = getStockByProDis($product_list['ID'],$request->distributor_id);
                $products[] = $product_list;
            }


        }

        if(!empty($_GET)){
            if(!empty($request->distributor_id)){
                $product_lists_result = $product_lists_result->appends($_GET);
            }
            return view('dashboard.store.distributor_stock',['distributors'=>$distributors,'shell_master_categories'=>$shell_master_categories,'categories'=>$categories,'manufacturers'=>$manufacturers,'brands'=>$brands,'pack_sizes'=>$pack_sizes,'products'=>$products,'selected'=>$selected,'product_lists_result'=>$product_lists_result]);
        }else{
            return view('dashboard.store.distributor_stock',['distributors'=>$distributors,'shell_master_categories'=>$shell_master_categories,'categories'=>$categories,'manufacturers'=>$manufacturers,'brands'=>$brands,'pack_sizes'=>$pack_sizes,'products'=>$products,'selected'=>$selected,'product_lists_result'=>$product_lists_result]);
        }
    }
    public function change_distributor($distributor_id){
        
        if($distributor_id == "all"){

            $shell_master_categories = DB::table('ClientMasterCategory')->orderBy('Name')->get();
            $shell_master_categories =  json_decode(json_encode($shell_master_categories), true);

            $categories = DB::table('Category')->orderBy('Name')->get();
            $categories = json_decode(json_encode($categories),true);

            $manufacturers = DB::select('select * from Supplier');
            $manufacturers = json_decode(json_encode($manufacturers),true);

            $brands = DB::table('Brand')->orderBy('Name')->get();
            $brands = json_decode(json_encode($brands),true);

            $pack_sizes = DB::table('pack_sizes')->orderBY('pack_size_name')->get();
            $pack_sizes = json_decode(json_encode($pack_sizes),true);

        }else{
            $shell_master_categories = DB::select("SELECT distinct ClientMasterCategory.ID,ClientMasterCategory.Name from `ClientMasterCategory` inner join `Product` on `Product`.`client_master_category_id` = `ClientMasterCategory`.`ID` inner join `SellerProduct` on `SellerProduct`.`Product_ID` = `Product`.`ID` where `SellerProduct`.`SellerID` = $distributor_id order by `ClientMasterCategory`.`Name` ");

            $shell_master_categories = json_decode(json_encode($shell_master_categories),true);

            $categories = DB::select("SELECT distinct Category.ID,Category.Name from `Category` inner join `Product` on `Product`.`category_id` = `Category`.`ID` inner join `SellerProduct` on `SellerProduct`.`Product_ID` = `Product`.`ID` where `SellerProduct`.`SellerID` = $distributor_id order by `Category`.`Name` ");

            $categories = json_decode(json_encode($categories),true);

            //$manufacturers = DB::select("SELECT distinct Supplier.ID, Supplier.Name FROM Supplier, Seller, SupplierSeller WHERE (SupplierSeller.SupplierID = Supplier.ID) AND (SupplierSeller.SellerID = $distributor_id) ORDER BY Supplier.Name ASC;");

            $manufacturers = DB::select("SELECT distinct Supplier.ID, Supplier.Name FROM Supplier, Seller, SellerProduct WHERE (SellerProduct.SupplierID = Supplier.ID) AND (SellerProduct.SellerID = $distributor_id) ORDER BY Supplier.Name ASC;");

            $manufacturers = json_decode(json_encode($manufacturers),true);


            $brands = DB::select("SELECT distinct Brand.ID,Brand.Name from `Brand` inner join `Product` on `Product`.`brand_id` = `Brand`.`ID` inner join `SellerProduct` on `SellerProduct`.`Product_ID` = `Product`.`ID` where `SellerProduct`.`SellerID` = $distributor_id order by `Brand`.`Name` ");

            $brands = json_decode(json_encode($brands),true);

            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name from `pack_sizes` inner join `Product` on `Product`.`pack_size_id` = `pack_sizes`.`pack_size_id` inner join `SellerProduct` on `SellerProduct`.`Product_ID` = `Product`.`ID` where `SellerProduct`.`SellerID` = $distributor_id order by `pack_sizes`.`pack_size_name` ");

            $pack_sizes = json_decode(json_encode($pack_sizes),true);
        }

        
        $data['shell_master_categories'] =  $shell_master_categories;
        $data['categories'] =  $categories;
        $data['manufacturers'] =  $manufacturers;
        $data['brands'] =  $brands;
        $data['pack_sizes'] =  $pack_sizes;

        return response()->json($data);

    }
    public function change_master_categ($master_category_id,$distributor_id){


        $data = [];

        if($distributor_id != "null"){
            $categories = DB::select("SELECT distinct Category.ID,Category.Name from Category,Product,ClientMasterCategory,SellerProduct,SupplierSeller WHERE (Product.category_id = Category.ID) AND  (ClientMasterCategory.ID = Product.client_master_category_id) AND (SellerProduct.Product_ID = Product.ID) AND (ClientMasterCategory.ID = $master_category_id) AND (SellerProduct.SellerID = $distributor_id) ORDER BY Category.Name ASC;");

            $categories = json_decode(json_encode($categories),true);


            $manufacturers = DB::select("SELECT distinct Supplier.ID, Supplier.Name FROM ClientMasterCategory, Product, Supplier, SellerProduct, SupplierSeller WHERE (ClientMasterCategory.ID = $master_category_id) AND (ClientMasterCategory.ID = Product.client_master_category_id)  AND (Product.ID = SellerProduct.Product_ID) AND (SellerProduct.SupplierID = Supplier.ID) AND (SupplierSeller.SupplierID = Supplier.ID) AND (SupplierSeller.SellerID = $distributor_id) ORDER BY Supplier.Name ASC;");

            $manufacturers = json_decode(json_encode($manufacturers),true);

            $brands = DB::select("SELECT distinct Brand.ID, Brand.Name FROM ClientMasterCategory, Product, Brand, SellerProduct WHERE (ClientMasterCategory.ID = $master_category_id) AND (ClientMasterCategory.ID = Product.client_master_category_id)  AND (Product.brand_id = Brand.ID) AND (SellerProduct.Product_ID = Product.ID) AND (SellerProduct.SellerID = $distributor_id) ORDER BY Brand.Name ASC;");

            $brands = json_decode(json_encode($brands),true);


            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id, pack_sizes.pack_size_name FROM ClientMasterCategory, Product, pack_sizes, SellerProduct WHERE (ClientMasterCategory.ID = $master_category_id) AND (ClientMasterCategory.ID = Product.client_master_category_id) AND (Product.pack_size_id = pack_sizes.pack_size_id) AND (SellerProduct.Product_ID = Product.ID) AND (SellerProduct.SellerID = $distributor_id) ORDER BY pack_sizes.pack_size_name ASC;");

            $pack_sizes = json_decode(json_encode($pack_sizes),true);
        }else{
            $categories = DB::select("SELECT distinct Category.ID,Category.Name from Category,Product,ClientMasterCategory,SellerProduct,SupplierSeller WHERE (Product.category_id = Category.ID) AND  (ClientMasterCategory.ID = Product.client_master_category_id) AND (SellerProduct.Product_ID = Product.ID) AND (ClientMasterCategory.ID = $master_category_id)  ORDER BY Category.Name ASC;");

            $categories = json_decode(json_encode($categories),true);


            $manufacturers = DB::select("SELECT distinct Supplier.ID, Supplier.Name FROM ClientMasterCategory, Product, Supplier, SellerProduct, SupplierSeller WHERE (ClientMasterCategory.ID = $master_category_id) AND (ClientMasterCategory.ID = Product.client_master_category_id)  AND (Product.ID = SellerProduct.Product_ID) AND (SellerProduct.SupplierID = Supplier.ID) AND (SupplierSeller.SupplierID = Supplier.ID)  ORDER BY Supplier.Name ASC;");

            $manufacturers = json_decode(json_encode($manufacturers),true);

            $brands = DB::select("SELECT distinct Brand.ID, Brand.Name FROM ClientMasterCategory, Product, Brand WHERE (ClientMasterCategory.ID = $master_category_id) AND (ClientMasterCategory.ID = Product.client_master_category_id) AND (Product.brand_id = Brand.ID) ORDER BY Brand.Name ASC;");

            $brands = json_decode(json_encode($brands),true);


            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id, pack_sizes.pack_size_name FROM ClientMasterCategory, Product, pack_sizes WHERE (ClientMasterCategory.ID = $master_category_id) AND (ClientMasterCategory.ID = Product.client_master_category_id) AND (Product.pack_size_id = pack_sizes.pack_size_id) ORDER BY pack_sizes.pack_size_name ASC;");

            $pack_sizes = json_decode(json_encode($pack_sizes),true);
        }


        

        // echo "<pre>";
        // print_r($brands);
        // die();

        $data['categories'] =  $categories;
        $data['manufacturers'] =  $manufacturers;
        $data['brands'] =  $brands;
        $data['pack_sizes'] =  $pack_sizes;
        return response()->json($data);

    }
    public function change_categ($category_id,$distributor_id){

        $data = [];


        $manufacturers = DB::select("SELECT DISTINCT  Supplier.ID,Supplier.Name FROM Supplier,Product, Category, SellerProduct, SupplierSeller WHERE (Category.ID = $category_id) AND (Category.ID = Product.category_id) AND (Product.ID = SellerProduct.Product_ID)  AND (SellerProduct.SupplierID = Supplier.ID) AND (SupplierSeller.SupplierID = Supplier.ID) AND (SupplierSeller.SellerID = $distributor_id) ORDER BY Supplier.Name ASC;");

        $manufacturers = json_decode(json_encode($manufacturers),true);

        $brands = DB::select("SELECT distinct Category.Name, Category.ID,Brand.Name, Brand.ID FROM Brand, Category, Product WHERE (Product.brand_id = Brand.ID) AND (Product.category_id = Category.ID) AND (Category.ID = $category_id) ORDER BY Brand.Name ASC;");

        $brands = json_decode(json_encode($brands),true);


        $pack_sizes = DB::select("SELECT distinct Category.ID, Category.Name, pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Category, Product WHERE (Category.ID = $category_id) AND (Product.category_id = Category.ID) AND (Product.pack_size_id = pack_sizes.pack_size_id) ORDER BY pack_sizes.pack_size_name ASC;");

        $pack_sizes = json_decode(json_encode($pack_sizes),true);

        // echo "<pre>";
        // print_r($brands);
        // die();

        $data['manufacturers'] =  $manufacturers;
        $data['brands'] =  $brands;
        $data['pack_sizes'] =  $pack_sizes;
        return response()->json($data);


    }

    public function change_manufacturer($manufacturer_id,$category_id){

        $data = [];
       
        if ($category_id != 'null'){
            $brands = DB::select("SELECT distinct Brand.Name, Brand.ID FROM Brand,Supplier, Product, Category, SellerProduct WHERE (Product.brand_id = Brand.ID) AND (Product.ID = SellerProduct.Product_ID) AND (SellerProduct.SupplierID = Supplier.ID)  AND (Supplier.ID = $manufacturer_id) AND (Product.category_id = Category.ID) AND (Category.ID = $category_id) ORDER BY Brand.Name ASC;");
        }
        else{
            $brands = DB::select("SELECT distinct Brand.Name, Brand.ID FROM Brand,Supplier, Product, SellerProduct WHERE (Product.brand_id = Brand.ID) AND (Product.ID = SellerProduct.Product_ID) AND (SellerProduct.SupplierID = Supplier.ID) AND (Supplier.ID = $manufacturer_id) ORDER BY Brand.Name ASC;");
        }

        $brands = json_decode(json_encode($brands),true);

        if ($category_id != 'null'){

            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Supplier, Product, Category,SellerProduct WHERE (Product.ID = SellerProduct.Product_ID) AND (SellerProduct.SupplierID = Supplier.ID)  AND  (Supplier.ID = $manufacturer_id) AND (Product.pack_size_id = pack_sizes.pack_size_id) AND (Category.ID = $category_id) AND (Category.ID = Product.category_id) ORDER BY pack_sizes.pack_size_name ASC;");

        }
        else{

            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Supplier,SellerProduct, Product WHERE (Product.ID = SellerProduct.Product_ID) AND (SellerProduct.SupplierID = Supplier.ID)  AND (Supplier.ID = $manufacturer_id)  AND (Product.pack_size_id = pack_sizes.pack_size_id) ORDER BY pack_sizes.pack_size_name ASC;");
        }

        $pack_sizes = json_decode(json_encode($pack_sizes),true);

        // echo "<pre>";
        // print_r($brands);
        // die();

        $data['brands'] =  $brands;
        $data['pack_sizes'] =  $pack_sizes;
        return response()->json($data);
    }
    public function change_brand($category_id,$manufacturer_id,$brand_id){

        $data = [];

        if ($category_id != 'null' && $manufacturer_id != 'null'){
            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Brand, Product, Supplier, Category WHERE (Brand.ID = $brand_id) AND (Product.brand_id = Brand.ID) AND (Product.pack_size_id = pack_sizes.pack_size_id) AND (Supplier.ID = $manufacturer_id) AND (Category.ID = $category_id) AND (Category.ID = Product.category_id) ORDER BY pack_sizes.pack_size_name ASC;");
        }

        if($category_id == 'null' && $manufacturer_id != 'null'){
            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Brand, Product, Supplier WHERE (Brand.ID = $brand_id) AND (Product.brand_id = Brand.ID) AND (Product.pack_size_id = pack_sizes.pack_size_id) AND (Supplier.ID = $manufacturer_id) ORDER BY pack_sizes.pack_size_name ASC;");
        }
        if($category_id != 'null' && $manufacturer_id == 'null'){
            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Brand, Product, Supplier, Category WHERE (Brand.ID = $brand_id) AND (Product.brand_id = Brand.ID) AND (Product.pack_size_id = pack_sizes.pack_size_id)  AND (Category.ID = $category_id) AND (Category.ID = Product.category_id) ORDER BY pack_sizes.pack_size_name ASC;");
        }
        if($category_id == 'null' && $manufacturer_id == 'null'){
            $pack_sizes = DB::select("SELECT distinct pack_sizes.pack_size_id,pack_sizes.pack_size_name FROM pack_sizes, Brand, Product WHERE (Brand.ID = $brand_id) AND (Product.brand_id = Brand.ID) AND (Product.pack_size_id = pack_sizes.pack_size_id) ORDER BY pack_sizes.pack_size_name ASC;");
        }

        $pack_sizes = json_decode(json_encode($pack_sizes),true);
        $data['pack_sizes'] =  $pack_sizes;
        return response()->json($data);
    }
    public function add_product_store(Request $request){
        // echo "<pre>";
        // var_dump($request->all()); die;

        $product_id_arr = $request->product_id;
        $quantity_arr = $request->quantity;
        $product_stock_arr = $request->product_stock;

        // echo "<pre>";
        // print_r($request->all());
        
        $count = 0;
        foreach($product_id_arr as $product_id){
            $quantity = (int)$request->quantity[$count];
            $product_stock = (int)$request->product_stock[$count];

            if($quantity > 0){
                 // $product_id = (int)$request->product_id;
                // $quantity = (int)$request->quantity;
                // $product_stock = (int)$request->product_stock;

                $product_info = DB::table('Product')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->get()->first();

                $product_info = json_decode(json_encode($product_info),true);

                $cart_products = Cart::getContent();

                $cart_products = json_decode(json_encode($cart_products),true);



                $check_quantity = $quantity;
                $already_added = 0;
                foreach($cart_products as $cart_item){
                    if($product_id == $cart_item['id']){
                        $already_added = 1;
                        $previous_quantity = trim($cart_item['quantity']);
                        $check_quantity = trim($check_quantity) + trim($cart_item['quantity']);
                    }
                }

                if($already_added == 1){
                    if(strpos($product_id,"_bck")){
                        Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $quantity, array());
                    }else{
                        if($check_quantity <= $product_stock){
                            Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $quantity, array());
                        }else{
                            if($previous_quantity < $product_stock){
                                $order_quantity = $product_stock - $previous_quantity;

                                Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $order_quantity, array());

                                $backorder_quantity = trim($quantity) - trim($order_quantity);

                                $product_id = $product_id . "_bck";

                                Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $backorder_quantity, array());
                            }else{

                                $product_id = $product_id . "_bck";

                                $backorder_quantity = trim($quantity);

                                // echo $backorder_quantity;
                                // echo $quantity;
                                // die("hello");

                                Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $backorder_quantity, array());
                            }

                        }
                    }
                }else{
                    if($check_quantity <= $product_stock){
                        Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $quantity, array());
                    }else{
                        if($product_stock==0) {
                            $product_id = $product_id . "_bck";
                            Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $quantity, array());
                        } else {
                            $order_quantity = $product_stock;
                            Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $order_quantity, array());

                            $backorder_quantity = $quantity - $product_stock;
                            $product_id = $product_id . "_bck";

                            Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $backorder_quantity, array());
                        }
                        
                    }
                }
            }
            $count++;
        }
        // echo $quantity;
        // die("hello");

       

        return redirect()->back();

    }
    public function update_product_store(Request $request){

        $product_id_arr = $request->product_id;
        $quantity_arr = $request->quantity;
        
        $count = 0;
        foreach($product_id_arr as $product_id){

            
            $product_stock = 0;
            $distributor_id = $request->distributor_id;
            $quantity = (int)$request->quantity[$count];

            if($quantity >= 0){
                $product_stock = getStockByProDis($product_id,$distributor_id);

                $cart_products = Cart::getContent();
                $cart_products = json_decode(json_encode($cart_products),true);

            if(strpos($product_id,"_bck")){
                    Cart::update($product_id,[
                        'quantity' => [
                            'relative' => false,
                            'value' => $quantity
                        ],
                    ]);
                }else{
                    $backorder_product_id = $product_id . "_bck";
                    $backorder_product_id_exist = 0;
                    $product_description = "";
                    $product_price = "";
                    $backorder_previous_quantity = 0;
                    // echo "<pre>";
                    // print_r($cart_products);
                    // die();
                    foreach($cart_products as $cart_item){
                        if($product_id == $cart_item['id']){
                            $previous_quantity = trim($cart_item['quantity']);
                            $check_quantity = trim($quantity);
                            $product_description = $cart_item['name'];
                            $product_price = $cart_item['price'];
                        }

                        if($backorder_product_id === $cart_item['id']){
                            $backorder_product_id_exist = 1;
                            $backorder_previous_quantity = trim($cart_item['quantity']);
                        }
                    };
                    if($check_quantity <= $product_stock){
                        Cart::update($product_id,[
                            'quantity' => [
                                'relative' => false,
                                'value' => $quantity
                            ],
                        ]);
                    }else{

                        Cart::update($product_id,[
                            'quantity' => [
                                'relative' => false,
                                'value' => $product_stock
                            ],
                        ]);
                        $backorder_quantity = ($check_quantity + $backorder_previous_quantity) - $product_stock;
                        if($backorder_product_id_exist == 1){
                            Cart::update($backorder_product_id,[
                                'quantity' => [
                                    'relative' => false,
                                    'value' => $backorder_quantity
                                ],
                            ]);
                        }else{
                            Cart::add($backorder_product_id, $product_description, $product_price, $backorder_quantity, array());
                        }

                    }
                }

            }
            $count++;

        }



        
        return redirect()->back();
    }
    public function delete_product_store(Request $request){
        $product_id = $request->product_id;
    //    echo $product_id;
    //    die();
        Cart::remove($product_id);

        return redirect()->back();
    }
    public function place_order_store(Request $request){
        $distributor_id = "";
        if(!empty($request->distributor_id)){
            $distributor_id = $request->distributor_id;
        }

        if(empty($distributor_id)){
            return redirect('create_store_order');
        }

        $last_order_info = [];
        $last_order_info = DB::table('orders')->latest('order_id')->first();

        $last_order_info = json_decode(json_encode($last_order_info),true);

        if(empty($last_order_info)){
            $last_order_info['order_id'] = 1;
        }

        $order_number = "ORD ". ($last_order_info['order_id'] + 1);

        // echo "<pre>";
        // print_r($last_order_info);
        // die();

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);


        $distributor_info = DB::table('Seller')->where('id',$distributor_id)->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);

        // print_r($distributor_info);
        // die();


        $cart_items = [];

        $cart_products = Cart::getContent();

        $cart_products = json_decode(json_encode($cart_products),true);




        // echo "<pre>";
        // print_r($cart_products);
        // die("hello");

        $total_amount_info = array(
            'total_excl_amount' => 0,
            'total_tax_amount' => 0,
            'total_order_amount' => 0,
            'total_backorder_amount' => 0
        );

        foreach($cart_products as $cart_product){
            
            if(strpos($cart_product['id'],"_return")){
                continue;
            }

            $product_id = $cart_product['id'];

            $product_info = DB::table('Product')->select('Product.*','Tax.tax_factor','SellerProduct.SellerCode')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->where('SellerProduct.SellerID',$distributor_id)->get()->first();

            //$product_info = DB::table('Product')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->get()->first();

            $product_info = json_decode(json_encode($product_info),true);


            $excl_amount = $product_info['price'] * $cart_product['quantity'];
            $total_amount_info['total_excl_amount'] = $total_amount_info['total_excl_amount'] + $excl_amount;

            $tax_amount = $product_info['tax_factor'] * 100 * $cart_product['quantity'];
            $total_amount_info['total_tax_amount'] = $total_amount_info['total_tax_amount'] + $tax_amount;

            $order_amount = $excl_amount + $tax_amount;


            $total_price = ($product_info['price'] * $cart_product['quantity']) +  ($product_info['tax_factor'] * 100 * $cart_product['quantity']);

            $product_info['total_price'] = 'R '.  number_format($total_price, 2, '.', ' ');

            $product_info['price'] = 'R '.  number_format($product_info['price'], 2, '.', ' ');
            $product_info['tax_factor'] = $product_info['tax_factor'] * 100 . '%';

            if($distributor_id > 0 && $distributor_id <= 7){
                $product_info['product_code_actual'] = $product_info['SellerCode'];
            }else{
                $product_info['product_code_actual'] = $product_info['SellerCode'];
            }

            $cart_product['back_order'] = "";
            if(strpos($cart_product['id'],"_bck")){
                $cart_product['back_order'] = "backorder";
                $total_amount_info['total_backorder_amount'] = $total_amount_info['total_backorder_amount'] + $order_amount;
            }else{
                $total_amount_info['total_order_amount'] = $total_amount_info['total_order_amount'] + $order_amount;
            }


            $cart_product['product_info'] = $product_info;

            $cart_items[] = $cart_product;
        }

        $total_amount_info['total_excl_amount'] = 'R '.  number_format($total_amount_info['total_excl_amount'], 2, '.', ' ');

        $total_amount_info['total_tax_amount'] = 'R '.  number_format($total_amount_info['total_tax_amount'], 2, '.', ' ');

        $total_amount_info['total_order_amount'] = 'R '.  number_format($total_amount_info['total_order_amount'], 2, '.', ' ');

        $total_amount_info['total_backorder_amount'] = 'R '.  number_format($total_amount_info['total_backorder_amount'], 2, '.', ' ');

        $delivery_info = DB::table("StoreDistributor")->where('Buyer_eR2mID','=',$store_info['ID'])->where('SellerID','=',$distributor_info['ID'])->get()->first();

        
        // echo "<pre>";
        // print_r($delivery_info);
        // die();

        $nod = date('Y-m-d', strtotime($delivery_info->nod));
        $ndd = date('Y-m-d', strtotime($delivery_info->ndd));
        
        $order_date = date('Y-m-d');

        $diff = strtotime($ndd) - strtotime($order_date);
        $min_diff = strtotime($ndd) - strtotime($nod);


        if($diff < 0){
            $diff = $diff + 7*3600*24;
        }

        if($min_diff < 0){
            $min_diff = $min_diff + 7*3600*24;
        }

        $diff  = floor($diff/3600/24);
        $min_diff  = floor($min_diff/3600/24);

        if($min_diff > $diff){
            $expected_delivery_date = date("j F Y",strtotime("+7 day", strtotime($ndd)));
        }else{
            $expected_delivery_date = date("j F Y",strtotime($ndd));
        }

        // echo "<pre>";
        // print_r($store_info);
        // print_r($distributor_info);
        // die();

        return view('dashboard.store.place_store_order',["cart_items"=>$cart_items,"total_amount_info"=>$total_amount_info,"store_info"=>$store_info,"distributor_info"=>$distributor_info,'order_number'=>$order_number,'expected_delivery_date'=>$expected_delivery_date]);
    }
    public function submit_place_order_store(Request $request){

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $cart_products = Cart::getContent();
        $cart_products = json_decode(json_encode($cart_products),true);

        $total_order_amount = 0;
        $total_backorder_amount = 0;

        $cart_items = [];
        $backorder_cart_items = [];

        foreach($cart_products as $cart_product){

            if(strpos($cart_product['id'],"_return")){
                continue;
            }elseif(strpos($cart_product['id'],"_bck")){
                $product_id = str_replace("_bck","",$cart_product['id']);

                $product_info = DB::table('Product')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->get()->first();

                $product_info = json_decode(json_encode($product_info),true);

                $cart_product['excl_amount'] = $product_info['price'] * $cart_product['quantity'];

                $cart_product['tax_amount'] = $product_info['tax_factor'];
                $cart_product['total_tax_amount'] = $product_info['tax_factor'] * 100 * $cart_product['quantity'];

                $cart_product['total_price'] = ($product_info['price'] * $cart_product['quantity']) +  ($product_info['tax_factor'] * 100 * $cart_product['quantity']);

                $total_backorder_amount = $total_backorder_amount + $cart_product['total_price'];

                $backorder_cart_items[] = $cart_product;
            }else{
                $product_id = $cart_product['id'];

                $product_info = DB::table('Product')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->get()->first();

                $product_info = json_decode(json_encode($product_info),true);

                $cart_product['excl_amount'] = $product_info['price'] * $cart_product['quantity'];

                $cart_product['tax_amount'] = $product_info['tax_factor'];
                $cart_product['total_tax_amount'] = $product_info['tax_factor'] * 100 * $cart_product['quantity'];

                $cart_product['total_price'] = ($product_info['price'] * $cart_product['quantity']) +  ($product_info['tax_factor'] * 100 * $cart_product['quantity']);

                $total_order_amount = $total_order_amount + $cart_product['total_price'];

                $cart_items[] = $cart_product;
            }



        }
        // echo "<pre>";
        // echo $total_order_amount;
        // print_r($cart_items);
        // echo $total_backorder_amount;
        // print_r($backorder_cart_items);
        // die();





        $distributor_id = $request->distributor_id;
        $distributor_name = $request->distributor_name;
        $distributor_order_number = $request->distributor_order_number;
        $order_number = $request->order_number;
        $backorder_number = "B".$order_number;
        $user_id = $user_info['user_id'];
        $seller_id = $request->sellerId;
        $user_seller_id = $request->userSellerId;
        $telephone1 = $request->telephone1;
        //$total = $request->total_order_amount;
       // $total_backorder_amount = $request->total_backorder_amount;
        $account = $request->account;
        $date = date("Y-m-d H:i:s");
        $status = "sent";
        $mod_date = strtotime($date."+ 3 days");
        $date_expected_delivery = date("Y-m-d  H:i:s",strtotime($request->expected_delivery_date));

        $order_id = "";
        $backorder_id = "";

        if(!empty($cart_items)){

            if(!$this->verifyStock($distributor_id,$cart_items)) {
                foreach($cart_items as $cart_item){
                    Cart::remove($cart_item['id']);
                }
                if(!empty($backorder_cart_items)){
                    foreach($backorder_cart_items as $cart_item){
                        Cart::remove($cart_item['id']);
                    }
                }
                Session::flash('flash_message', 'Some products is out of stock. Please revise your order.');
                Session::flash('flash_type', 'alert-danger');
                return redirect('/create_store_order');
            }

            $insert_order_data = array(
                'order_number' => trim($order_number),
                'user_seller_id' => trim($user_seller_id),
                'user_id' => trim($user_id),
                'distributor_id' => trim($distributor_id),
                'seller_id' => trim($seller_id),
                'date' => trim($date),
                'status' => trim($status),
                'total' => trim($total_order_amount),
                'distributor_name' => trim($distributor_name),
                'distributor_order_number' => trim($distributor_order_number), 'expected_delivery_date' => trim($date_expected_delivery),
                'account' => trim($account)
            );

            $order_id = DB::table('orders')->insertGetId($insert_order_data);

            $insert_order_products = [];

            if($order_id){
                $line_no = 0;
                foreach($cart_items as $cart_item){
                    $line_no++;
                    $order_product = array(
                        'order_id' => $order_id,
                        'order_line_number' => $line_no,
                        'product_id' => $cart_item['id'],
                        'qty' => $cart_item['quantity'],
                        'price' => $cart_item['price'],
                        'tax' => $cart_item['tax_amount'] * 100,
                        'subtotal' => $cart_item['total_price'],
                    );
                    $insert_order_products[] = $order_product;
                    Cart::remove($cart_item['id']);
                }
                DB::table('order_lines')->insert($insert_order_products);
                $this->updateStock($distributor_id,$cart_items);
            }
        } 

        if(!empty($backorder_cart_items)){
            $insert_backorder_data = array( 
                'back_order_number' => trim($backorder_number),
                'user_seller_id' => trim($user_seller_id),
                'user_id' => trim($user_id),
                'distributor_id' => trim($distributor_id),
                'seller_id' => trim($seller_id),
                'date' => trim($date),
                'status' => trim($status),
                'total' => trim($total_backorder_amount),
                'distributor_name' => trim($distributor_name),
                'distributor_order_number' => trim($distributor_order_number),
                'account' => trim($account)
            );

            $backorder_id = DB::table('back_orders')->insertGetId($insert_backorder_data);
            $insert_backorder_products = [];

            if($backorder_id){
                $backorder_line_no = 0;
                foreach($backorder_cart_items as $cart_item){
                    $backorder_line_no++;
                    $backorder_product = array(
                        'back_order_id' => $backorder_id,
                        'back_order_line_number' => $backorder_line_no,
                        'product_id' => str_replace("_bck","",$cart_item['id']),
                        'qty' => $cart_item['quantity'],
                        'price' => $cart_item['price'],
                        'tax' => $cart_item['tax_amount'] * 100,
                        'subtotal' => $cart_item['total_price'],
                    );
                    $insert_backorder_products[] = $backorder_product;
                    Cart::remove($cart_item['id']);
                }
                DB::table('back_order_lines')->insert($insert_backorder_products);

            }
        }

        if($order_id != ""){
            $this->sendOrderEmail($order_id);
        }
        if($backorder_id != ""){
            $this->sendBackOrderEmail($backorder_id);
        }


        if($order_id != ""){
            return redirect('/order_history_store');
        }elseif($backorder_id != ""){
            return redirect('/backorder_history_store');
        }else{
            return redirect('/order_history_store');
        }

    }

    protected function verifyStock($distributor_id,$items) {
        foreach($items as $item) {
            $current_quantity = getStockByProDis($item['id'], $distributor_id);
            if($current_quantity>=$item['quantity']) {
                return true;
            }
            return false;
        }
    }

    protected function updateStock($distributor_id,$items) {
        foreach($items as $item) {
            $current_quantity = getStockByProDis($item['id'], $distributor_id);
            if($current_quantity>=$item['quantity']) {
                $current_quantity = $current_quantity-$item['quantity'];
                setStockByProDis($item['id'],$distributor_id,$current_quantity);
            }
        }
    }

    protected function sendOrderEmail($order_id="") {
        //$order_id="152";

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_info = DB::table('orders')->where('order_id',$order_id)->where('orders.user_id',$user_info['user_id'])->get()->first();
        $order_info = json_decode(json_encode($order_info),true);

        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();
        $store_info = json_decode(json_encode($store_info),true);

        $distributor_info = DB::table('Seller')
        ->join('User','User.seller_id','=','Seller.id')
        ->where('Seller.id',$order_info['distributor_id'])
        ->select('User.email', 'Seller.*')
        ->get()->first();
        $distributor_info = json_decode(json_encode($distributor_info),true);


        $order_products_info = DB::table('order_lines')->select('order_lines.*','Product.*','SellerProduct.SellerCode')->join('Product','Product.ID','=','order_lines.product_id')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->where('order_lines.order_id',$order_id)->where('SellerProduct.SellerID',$order_info['distributor_id'])->get();
        $order_products_info = json_decode(json_encode($order_products_info),true);

        $orderData = array('store_info'=>$store_info,'order_info'=>$order_info,'distributor_info'=>$distributor_info,'order_products_info'=>$order_products_info);

        $orderData['userName'] = $distributor_info['Name'];
        $orderData['email'] = $distributor_info['email'];
        $orderData['subject'] = "Invoice # ".$order_info['order_number'];

        
        Mail::send('/dashboard/emails/order', $orderData, function($message) use ($orderData) {
            $message->from('no-reply@qualhon.org', 'stockit');
            $message->to($orderData['email'],$orderData['userName']);
            $message->subject($orderData['subject']);
        
            $pdf = PDF::loadView('/dashboard/emails/orderPDF', $orderData);
            $message->attachData($pdf->output(), "Invoice # ".$orderData['order_info']['order_number']);
            //$message->setBody('<h3>Hello</h3>', 'text/html');
        });

        if (Mail::failures()) {
            
        }
        
        //return view('dashboard.emails.orderPDF',$orderData);
    }

    protected function sendBackOrderEmail($order_id="") {
        //$order_id="17";

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_info = DB::table('back_orders')->where('back_order_id',$order_id)->where('back_orders.user_id',$user_info['user_id'])->get()->first();
        $order_info = json_decode(json_encode($order_info),true);
        
        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();
        $store_info = json_decode(json_encode($store_info),true);

        $distributor_info = DB::table('Seller')
        ->join('User','User.seller_id','=','Seller.id')
        ->where('Seller.id',$order_info['distributor_id'])
        ->select('User.email', 'Seller.*')
        ->get()->first();
        $distributor_info = json_decode(json_encode($distributor_info),true);

        $order_products_info = DB::table('back_order_lines')->select('back_order_lines.*','Product.*','SellerProduct.SellerCode')->join('Product','Product.ID','=','back_order_lines.product_id')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->where('back_order_lines.back_order_id',$order_id)->where('SellerProduct.SellerID',$order_info['distributor_id'])->get();
 
        $order_products_info = json_decode(json_encode($order_products_info),true);

        $orderData = array('store_info'=>$store_info,'order_info'=>$order_info,'distributor_info'=>$distributor_info,'order_products_info'=>$order_products_info);

        $orderData['userName'] = $distributor_info['Name'];
        $orderData['email'] = $distributor_info['email'];
        $orderData['subject'] = "Invoice # ".$order_info['back_order_number'];
        
        Mail::send('/dashboard/emails/backorder', $orderData, function($message) use ($orderData) {
            $message->from('no-reply@qualhon.org', 'stockit');
            $message->to($orderData['email'],$orderData['userName']);
            $message->subject($orderData['subject']);
        
            $pdf = PDF::loadView('/dashboard/emails/backorderPDF', $orderData);
            $message->attachData($pdf->output(), "Invoice # ".$orderData['order_info']['back_order_number']);
            //$message->setBody('<h3>Hello</h3>', 'text/html');
        });

        if (Mail::failures()) {
            
        }
        //return view('dashboard.emails.backorderPDF',$orderData);
    }

    public function testEmail(){
        
        $order_id="192";

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_info = DB::table('back_orders')->where('back_order_id',$order_id)->where('back_orders.user_id',$user_info['user_id'])->get()->first();
        $order_info = json_decode(json_encode($order_info),true);

       
        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();
        $store_info = json_decode(json_encode($store_info),true);

        $distributor_info = DB::table('Seller')
        ->join('User','User.seller_id','=','Seller.id')
        ->where('Seller.id',$order_info['distributor_id'])
        ->select('User.email', 'Seller.*')
        ->get()->first();
        $distributor_info = json_decode(json_encode($distributor_info),true);
 
        $order_products_info = DB::table('back_order_lines')->select('back_order_lines.*','Product.*','SellerProduct.SellerCode')->join('Product','Product.ID','=','back_order_lines.product_id')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->where('back_order_lines.back_order_id',$order_id)->where('SellerProduct.SellerID',$order_info['distributor_id'])->get();
        $order_products_info = json_decode(json_encode($order_products_info),true);

        // echo "<pre>";
        // print_r($order_products_info);
        // die();

        $orderData = array('store_info'=>$store_info,'order_info'=>$order_info,'distributor_info'=>$distributor_info,'order_products_info'=>$order_products_info);

        $orderData['userName'] = $distributor_info['Name'];
        $orderData['email'] = $distributor_info['email'];
        $orderData['subject'] = "Invoice # ".$order_info['back_order_number'];

        return view('dashboard.emails.backorderPDF',$orderData);
    }
    public function order_invoice(){

    }
    public function order_history_store(){

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_history_data = DB::table('orders')
        ->join('User','User.user_id','=','orders.user_id')
        ->join('order_status','order_status.order_status_id','=','orders.order_status_id')
        ->select('orders.*', 'order_status.*')
        ->where('User.user_id',$user_info['user_id'])
        ->orderBy('date', 'desc')
        ->Paginate(10);

        $order_history_list_tmp = json_decode(json_encode($order_history_data),true);
        $order_history_list = $order_history_list_tmp['data'];

        // echo "<pre>";
        // print_r($order_history_list);
        // die();


        return view('dashboard/store/order_history_store',["order_history_data"=>$order_history_data,"order_history_list"=>$order_history_list]);
    }
    public function view_order_detail_store($order_id){


        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_info = DB::table('orders')->where('order_id',$order_id)->where('orders.user_id',$user_info['user_id'])->get()->first();

        $order_info = json_decode(json_encode($order_info),true);

        if(empty($order_info)){
            return redirect('/order_history_store');
        }



        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);




        
        $distributor_info = DB::table('Seller')->where('id',$order_info['distributor_id'])->get()->first();
        
        $distributor_info = json_decode(json_encode($distributor_info),true);

        $order_products_info = DB::table('order_lines')->join('Product','Product.ID','=','order_lines.product_id')->where('order_lines.order_id',$order_id)->get();

        $order_products_info = json_decode(json_encode($order_products_info),true);

        $order_history_info = DB::table('order_history')
        ->join('order_status','order_status.order_status_id','=','order_history.order_status_id')
        ->where('order_history.order_id','=',$order_id)->get();
        $order_history_info = json_decode(json_encode($order_history_info),true);

        // print_r($order_info);
        // die();

        return view('dashboard.store.view_order_detail_store',['store_info'=>$store_info,'order_info'=>$order_info,'distributor_info'=>$distributor_info,'order_products_info'=>$order_products_info,'order_history'=>$order_history_info]);
    }

    public function backorder_history_store(){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $backorder_history_data = DB::table('back_orders')
        ->join('User','User.user_id','=','back_orders.user_id')
        ->join('back_order_status','back_order_status.order_status_id','=','back_orders.order_status_id')
        ->select('back_orders.*', 'back_order_status.*')
        ->where('User.user_id',$user_info['user_id'])
        ->orderBy('date', 'desc')
        ->Paginate(10);

        $backorder_history_list_tmp = json_decode(json_encode($backorder_history_data),true);
        $backorder_history_list = $backorder_history_list_tmp['data'];


        // echo "<pre>";
        // print_r($order_history_list);
        // die();


        return view('dashboard/store/backorder_history_store',["backorder_history_data"=>$backorder_history_data,"backorder_history_list"=>$backorder_history_list]);

    }
    public function view_backorder_detail_store($backorder_id){


        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $backorder_info = DB::table('back_orders')->where('back_order_id',$backorder_id)->where('back_orders.user_id',$user_info['user_id'])->get()->first();

        $backorder_info = json_decode(json_encode($backorder_info),true);

        if(empty($backorder_info)){
            return redirect('/backorder_history_store');
        }



        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);

        // print_r($store_info);
        // die();




        $distributor_info = DB::table('Seller')->where('id',$backorder_info['distributor_id'])->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);

        $backorder_products_info = DB::table('back_order_lines')->join('Product','Product.ID','=','back_order_lines.product_id')->where('back_order_lines.back_order_id',$backorder_id)->get();
        $backorder_products_info = json_decode(json_encode($backorder_products_info),true);

        $order_history_info = DB::table('back_order_history')
        ->join('back_order_status','back_order_status.order_status_id','=','back_order_history.order_status_id')
        ->where('back_order_history.order_id','=',$backorder_id)->get();
        $order_history_info = json_decode(json_encode($order_history_info),true);
        
        //print_r($order_history_info); die();

        return view('dashboard.store.view_backorder_detail_store',['store_info'=>$store_info,'backorder_info'=>$backorder_info,'distributor_info'=>$distributor_info,'backorder_products_info'=>$backorder_products_info,'back_order_history'=>$order_history_info]);
    }
    public function create_return_store(Request $request){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_history_data = DB::table('orders')
        ->join('User','User.user_id','=','orders.user_id')
        ->join('order_status','order_status.order_status_id','=','orders.order_status_id')
        ->select('orders.*', 'order_status.*')
        ->where('User.user_id',$user_info['user_id'])
        ->where('order_status.order_status_id','5')
        ->orderBy('date', 'desc')
        ->Paginate(10);

        $order_history_list_tmp = json_decode(json_encode($order_history_data),true);
        $order_history_list = $order_history_list_tmp['data'];

        // echo "<pre>";
        // print_r($order_history_list);
        // die();


        return view('dashboard/store/create_return_store',["order_history_data"=>$order_history_data,"order_history_list"=>$order_history_list]);
    }
    public function add_return_product_store(Request $request){
        
        $product_id = (int)$request->product_id;
        $quantity = (int)$request->quantity;
        $product_stock = (int)$request->product_stock;
        $uor = $request->uor;
        $reason = $request->reason;
        $notes = $request->notes;


        $product_info = DB::table('Product')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$product_id)->get()->first();

        $product_info = json_decode(json_encode($product_info),true);

        $cart_products = Cart::getContent();

        $cart_products = json_decode(json_encode($cart_products),true);

        $check_quantity = $quantity;
        $already_added = 0;
        $product_id = $product_id."_return";
        foreach($cart_products as $cart_item){
            if($product_id == $cart_item['id']){
                $already_added = 1;
                $previous_quantity = trim($cart_item['quantity']);
                $check_quantity = trim($check_quantity) + trim($cart_item['quantity']);
            }
        }

        if($already_added == 1){
                Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $quantity, array('uor'=>$uor,'reason'=>$reason,'notes'=>$notes));
        }else{
            Cart::add($product_id, $product_info['ScProductDescription'], $product_info['price'], $quantity, array('uor'=>$uor,'reason'=>$reason,'notes'=>$notes));
        }

        return redirect()->back();

    }
    public function update_return_product_store(Request $request){
        $product_id = $request->product_id;
        $quantity = $request->quantity;
        $distributor_id = $request->distributor_id;

        $uor = $request->uor;
        $reason = $request->reason;
        $notes = $request->notes;

        $product_stock = getStockByProDis($product_id,$distributor_id);

        $cart_products = Cart::getContent();
        $cart_products = json_decode(json_encode($cart_products),true);

       if(strpos($product_id,"_return")){
            Cart::update($product_id,[
                'quantity' => [
                    'relative' => false,
                    'value' => $quantity
                ],'attributes' => [
                    'uor'=>$uor,
                    'reason'=>$reason,
                    'notes'=>$notes
                ]
            ]);
        }

        return redirect()->back();
    }
    public function place_return_store($order_id){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_info = DB::table('orders')->where('order_id',$order_id)->where('orders.user_id',$user_info['user_id'])->get()->first();

        $order_info = json_decode(json_encode($order_info),true);

        if(empty($order_info)){
            return redirect('/create_return_store');
        }

        $last_order_info = [];
        $last_order_info = DB::table('returns')->latest('return_id')->first();

        $last_order_info = json_decode(json_encode($last_order_info),true);

        if(empty($last_order_info)){
            $last_order_info['return_id'] = 1;
        }

        $return_number = "RET ". ($last_order_info['return_id'] + 1);


        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);




        $distributor_info = DB::table('Seller')->where('ID',$order_info['distributor_id'])->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);


        $order_products_info = DB::table('order_lines')->join('Product','Product.ID','=','order_lines.product_id')->where('order_lines.order_id',$order_id)->get();

        $order_products_info = json_decode(json_encode($order_products_info),true);

        $uors = DB::table('units_of_return')->get();
        $uors = json_decode(json_encode($uors),true);

        $reasons = DB::table('reasons')->get();
        $reasons = json_decode(json_encode($reasons),true);


        return view('dashboard.store.place_store_return',['store_info'=>$store_info,'order_info'=>$order_info,'distributor_info'=>$distributor_info,'order_products_info'=>$order_products_info,'return_number'=>$return_number,'uors'=>$uors,'reasons'=>$reasons]);

    }
    public function submit_place_return_store(Request $request){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);
       
        $selected_product = $request->selected_product;

        if(!empty($selected_product)){

        }else{
            if(empty($request->distributor_id) && isset($_GET['distributor_id'])){
                Session::flash('flash_message', 'Please select any distributor!');
                Session::flash('flash_type', 'alert-danger');
            }
            return redirect()->back();
        }

        $cart_items = [];

        foreach($selected_product as $k=>$selected_product_id){

            $cart_product = [];

            $cart_product['product_id'] = $selected_product_id;
            $cart_product['quantity'] = $request->quantity[$k];
            $cart_product['uor'] = getReturnUOR($request->uors[$k]);
            $cart_product['reason'] = getReturnReason($request->reasons[$k]);
            $cart_product['notes'] = $request->notes[$k];


            $product_id = $selected_product_id;

            $product_info = DB::table('Product')->join('Tax','Tax.tax_id','=','Product.tax_id')->where('Product.ID',$selected_product_id)->get()->first();

            $product_info = json_decode(json_encode($product_info),true);

            $cart_product['product_info'] = $product_info;
            $cart_items[] = $cart_product;

        }

        $distributor_id = $request->distributor_id;
        $distributor_name = $request->distributor_name;
        $distributor_order_number = $request->distributor_order_number;
        $return_number = $request->return_number;
        $user_id = $user_info['user_id'];
        $seller_id = $request->sellerId;
        $user_seller_id = $request->userSellerId;
        $telephone1 = $request->telephone1;
    
        $account = $request->account;
        $date = date("Y-m-d H:i:s");
        $status = "sent";

        $return_id = "";

        if(!empty($cart_items)){

            $insert_return_data = array(
                'return_number' => trim($return_number),
                'user_seller_id' => trim($user_seller_id),
                'user_entity_id' => "1",
                'user_id' => trim($user_id),
                'seller_id' => trim($seller_id),
                'user_seller_id' => trim($user_seller_id),
                'distributor_id' => trim($distributor_id),
                'account' => trim($account),
                'date' => trim($date),
                'status' => trim($status),
                'distributor_name' => trim($distributor_name),
                'sage_id' => ""
            );

            $return_id = DB::table('returns')->insertGetId($insert_return_data);

            $insert_return_products = [];

            if($return_id){
                foreach($cart_items as $cart_item){
                    $return_product = array(
                        'return_id' => $return_id,
                        'product_id' => $cart_item['product_id'],
                        'qty' => $cart_item['quantity'],
                        'uom' => $cart_item['uor'],
                        'reason' => $cart_item['reason'],
                        'notes' => $cart_item['notes']
                    );
                    $insert_return_products[] = $return_product;
                }
                DB::table('return_lines')->insert($insert_return_products);
            }
        }

       // die("hello");
        if($return_id != ""){
            $this->sendReturnEmail($return_id);
            return redirect('/return_history_store');
        }
    }
    protected function sendReturnEmail($return_id="") {
        //$return_id="96";

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $return_info = DB::table('returns')->where('return_id',$return_id)->where('returns.user_id',$user_info['user_id'])->get()->first();
        $return_info = json_decode(json_encode($return_info),true);

        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();
        $store_info = json_decode(json_encode($store_info),true);

        $distributor_info = DB::table('Seller')
        ->join('User','User.seller_id','=','Seller.id')
        ->where('Seller.id',$return_info['distributor_id'])
        ->select('User.email', 'Seller.*')
        ->get()->first();
        $distributor_info = json_decode(json_encode($distributor_info),true);

        $return_products_info = DB::table('return_lines')->select('return_lines.*','Product.*','SellerProduct.SellerCode')->join('Product','Product.ID','=','return_lines.product_id')->join('SellerProduct','SellerProduct.Product_ID','Product.ID')->where('return_lines.return_id',$return_id)->where('SellerProduct.SellerID',$return_info['distributor_id'])->get();
        $return_products_info = json_decode(json_encode($return_products_info),true);

        $returnData = array('store_info'=>$store_info,'return_info'=>$return_info,'distributor_info'=>$distributor_info,'return_products_info'=>$return_products_info);

        $returnData['userName'] = $distributor_info['Name'];
        $returnData['email'] = $distributor_info['email'];
        $returnData['subject'] = "Invoice # ".$return_info['return_number'];

        
        Mail::send('/dashboard/emails/return', $returnData, function($message) use ($returnData) {
            $message->from('no-reply@qualhon.org', 'stockit');
            $message->to($returnData['email'],$returnData['userName']);
            $message->subject($returnData['subject']);
        
            $pdf = PDF::loadView('/dashboard/emails/returnPDF', $returnData);
            $message->attachData($pdf->output(), "Invoice # ".$returnData['return_info']['return_number']);
            //$message->setBody('<h3>Hello</h3>', 'text/html');
        });

        if (Mail::failures()) {
            
        }
        
        // return view('dashboard.emails.returnPDF',$returnData);
    }
    public function return_history_store(){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $return_history_data = DB::table('returns')
        ->join('User','User.user_id','=','returns.user_id')
        ->select('returns.*')
        ->where('User.user_id',$user_info['user_id'])
        ->orderBy('date', 'desc')
        ->Paginate(10);

        $return_history_list_tmp = json_decode(json_encode($return_history_data),true);
        $return_history_list = $return_history_list_tmp['data'];


        return view('dashboard/store/return_history_store',["return_history_data"=>$return_history_data,"return_history_list"=>$return_history_list]);

    }
    public function view_return_detail_store($return_id){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $return_info = DB::table('returns')->where('return_id',$return_id)->where('returns.user_id',$user_info['user_id'])->get()->first();

        $return_info = json_decode(json_encode($return_info),true);

        if(empty($return_info)){
            return redirect('/return_history_store');
        }



        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$user_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);




        $distributor_info = DB::table('Seller')->where('id',$return_info['distributor_id'])->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);

        $return_products_info = DB::table('return_lines')->join('Product','Product.ID','=','return_lines.product_id')->where('return_lines.return_id',$return_id)->get();

        $return_products_info = json_decode(json_encode($return_products_info),true);


        // print_r($order_info);
        // die();

        return view('dashboard.store.view_return_detail_store',['store_info'=>$store_info,'return_info'=>$return_info,'distributor_info'=>$distributor_info,'return_products_info'=>$return_products_info]);
    }
    public function reports_store(){
        return view('dashboard/store/reports_store');
    }
    public function import_data(){
        $path = url('assets/files/store_distributor_data.csv');
        echo $path;

        $fileD = fopen($path,"r");
        $column=fgetcsv($fileD);
        while(!feof($fileD)){
            $rowData[]=fgetcsv($fileD);
        }
        echo "<pre>";
        

        $info_array = [];
        $match_array = [];

        foreach($rowData as $row){
            
            $buyer_id = trim($row[0]);
            $store_name = trim($row[1]);
            $address = trim($row[2]);
            $region = trim($row[3]);
            $distributor_name = trim($row[4]); 

            $store_info = DB::table('Buyer_Client')->where('ID',$buyer_id)->first();

            $store_info = json_decode(json_encode($store_info),true);

            if(!empty($store_info)){
                $user_info = DB::table('User')->where('user_name',str_replace(" ","",$store_info['eR2m_Buyer_Name']))->first();
                $user_info = json_decode(json_encode($user_info),true);

                $distributor_info = DB::table('Seller')->where('brief_name',$distributor_name)->first();
                $distributor_info = json_decode(json_encode($distributor_info),true);

                $info_array['distributor_id'] = '';
                if(!empty($distributor_info)){
                    $info_array['distributor_id'] = $distributor_info['id'];
                }
                
                $info_array['store_id'] = $store_info['ID'];
                $info_array['user_store_id'] = '';
                if(!empty($user_info)){
                    $info_array['user_store_id'] = $user_info['store_id'];
                    $info_array['user_id'] = $user_info['user_id'];
                }

                if($info_array['store_id'] != $info_array['user_store_id']){
                   // DB::table('stores')->where('eR2m_Buyer_Name',$store_info['eR2m_Buyer_Name'])->update(['ID'=>$user_info['store_id']]);
                  // echo "mismatch";
                }
               // print_r($info_array);
                
            }
             $match_array[] =  $info_array;
            
        }

        $new_array = [];
        $tmp_array = [];
        foreach ( $match_array AS $key => $value ) { 
            if ( !in_array($value['user_id'], $tmp_array) ) { 
                $tmp_array[] = $value['user_id']; 
                $new_array[$key] = $value; 
            } 
        } 
        print_r($new_array);
        // foreach($match_array as $arr){
        //     $insert_data = array(
        //         'ID_store' => $arr['store_id'],
        //         'ID_distributor' => $arr['distributor_id']
        //     );
        //     DB::table('StoreDistributor')->insert($insert_data);
        // }
        // $

        foreach($new_array as $arr){
            $insert_data = array(
                'store_id' => $arr['store_id'],
                'user_id' => $arr['user_id']
            );
            // DB::table('user_stores')->insert($insert_data);
        }
    }
    public function check_cart(){

        $cart_products = Cart::getContent();
        $cart_products = json_decode(json_encode($cart_products),true);

        $result = array(
            'cart_products_count' => count($cart_products)
        );

        return response()->json($result);
    }
    public function clear_cart(){

        $cart_products = Cart::getContent();
        $cart_products = json_decode(json_encode($cart_products),true);

        if(!empty($cart_products)){
            foreach($cart_products as $cart_item){
                Cart::remove($cart_item['id']);
            }
        }
    }
    public function test(){
        $nod = date('Y-m-d', strtotime("tuesday"));
        $ndd = date('Y-m-d', strtotime("thursday"));
        
        $order_date = date('Y-m-d');
        
        echo "Nod Date : " . $nod . "<br>";
        echo "NDD Date : " . $ndd . "<br>";

        $diff = strtotime($ndd) - strtotime($order_date);
        $min_diff = strtotime($ndd) - strtotime($nod);


        if($diff < 0){
            $diff = $diff + 7*3600*24;
        }

        if($min_diff < 0){
            $min_diff = $min_diff + 7*3600*24;
        }

        $diff  = floor($diff/3600/24);
        $min_diff  = floor($min_diff/3600/24);

        echo "Difference : " . $diff . "<br>";
        echo "Min Difference : " . $min_diff . "<br>";

        if($min_diff > $diff){
            echo "Order Delivered on :" . date("Y-m-d",strtotime("+7 day", strtotime($ndd)));
        }else{
            echo "Order Delivered on :" . $ndd;
        }
    }
   

}

?>

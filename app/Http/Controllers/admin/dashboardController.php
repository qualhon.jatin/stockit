<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class dashboardController extends Controller
{
    public function __construct(){ 
        
    }

    public function index() {

        // die("success");
        $total_orders = DB::table('orders')->count();
        $total_back_orders = DB::table('back_orders')->count();
        
        $data = array(
            'orders' => $total_orders,
            'total_back_orders' => $total_back_orders,
            'total_orders' => ($total_orders+$total_back_orders)
        );
        return view('dashboard.homepage',$data);
    }

    public function chartData(Request $request) {
        if($request->range=='today') {
            $this->today();
        } else if($request->range=='week') {
            $this->week();
        } else if($request->range=='month') {
             $this->month();
        } else if($request->range=='year') {
             $this->year();
        } else if($request->range=='custom') {
            $this->custom($request->years);
        }
    }

    protected function today() {
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $labels = array();
        $order_data = array();
        $back_order_data = array();
        for ($i = 0; $i < 24; $i++) {
            $labels[] = (string)$i;
        }

        for ($i = 0; $i < 24; $i++) {
			$order_data[$i] = array(
				'hour'  => $i,
				'total' => 0
            );
            $back_order_data[$i] = array(
				'hour'  => $i,
				'total' => 0
			);
		}
        $today = date('Y-m-d');
        
        $orders = DB::select(DB::raw("SELECT COUNT(*) AS total, HOUR(date) AS hour FROM `orders` WHERE `user_id`='".$user_info['user_id']."' AND DATE(date) = DATE('".$today."') GROUP BY HOUR(date) ORDER BY date ASC "));
        foreach($orders as $order) {
            $order_data[$order->hour] = array(
                'hour' => $order->hour,
                'total' => $order->total
            );
        }

        $back_orders = DB::select(DB::raw("SELECT COUNT(*) AS total, HOUR(date) AS hour FROM `back_orders` WHERE `user_id`='".$user_info['user_id']."' AND DATE(date) = DATE('".$today."') GROUP BY HOUR(date) ORDER BY date ASC "));
        foreach($back_orders as $order) {
            $back_order_data[$order->hour] = array(
                'hour' => $order->hour,
                'total' => $order->total
            );
        }

        $data['orders'] = array('labels'=>$labels,'data'=>$order_data);
        $data['back_orders'] = array('labels'=>$labels,'data'=>$back_order_data);
        echo json_encode($data);
    }

    protected function week() {
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $labels = array();
        $order_data = array();
        $back_order_data = array();

        $date_start = strtotime('-' . date('w') . ' days');
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', $date_start + ($i * 86400));
            //$labels[] = array(date('W', strtotime($date)), date('D', strtotime($date)));
            $labels[] = date('D', strtotime($date));
            $order_data[date('w', strtotime($date))] = array(
                'day'   => date('D', strtotime($date)),
				'total' => 0
            );
            $back_order_data[date('w', strtotime($date))] = array(
                'day'   => date('D', strtotime($date)),
				'total' => 0
            );
        }

        $orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `orders` WHERE `user_id`='".$user_info['user_id']."' AND DATE(date) >= DATE('".date('Y-m-d', $date_start)."') GROUP BY DAYNAME(date)"));
        //print_r($orders); die;
        foreach ($orders as $order) {
            $order_data[date('w', strtotime($order->date))] = array(
                'day'   => date('D', strtotime($order->date)),
				'total' => $order->total
            );
        }

        $back_orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `back_orders` WHERE `user_id`='".$user_info['user_id']."' AND DATE(date) >= DATE('".date('Y-m-d', $date_start)."') GROUP BY DAYNAME(date)"));
        foreach ($orders as $order) {
            $back_order_data[date('w', strtotime($order->date))] = array(
                'day'   => date('D', strtotime($order->date)),
				'total' => $order->total
            );
        }
        //print_r($order_data); die;
        $data['orders'] = array('labels'=>$labels,'data'=>$order_data);
        $data['back_orders'] = array('labels'=>$labels,'data'=>$back_order_data);
        echo json_encode($data);
        
    }

    protected function month() {
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $labels = array();
        $order_data = array();
        $back_order_data = array();
        for ($i = 1; $i <= date('t'); $i++) {
            $date = date('Y') . '-' . date('m') . '-' . $i;
            $labels[] = date('j', strtotime($date));
            $order_data[date('j', strtotime($date))] = array(
				'day'   => date('d', strtotime($date)),
				'total' => 0
            );
            $back_order_data[date('j', strtotime($date))] = array(
				'day'   => date('d', strtotime($date)),
				'total' => 0
			);
        }

        $where_date = date('Y') . '-' . date('m') . '-1';
        $orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `orders` WHERE `user_id`='".$user_info['user_id']."' AND DATE(date) >= '" .$where_date. "' GROUP BY DATE(date) "));
        
        foreach ($orders as $order) {
            $order_data[date('j', strtotime($order->date))] = array(
                'day'   => date('j', strtotime($order->date)),
				'total' => $order->total
            );
        }

        $orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `back_orders` WHERE `user_id`='".$user_info['user_id']."' AND DATE(date) >= '" .$where_date. "' GROUP BY DATE(date) "));
        foreach ($orders as $order) {
            $back_order_data[date('j', strtotime($order->date))] = array(
                'day'   => date('j', strtotime($order->date)),
				'total' => $order->total
            );
        }
        //print_r($order_data); die;
        $data['orders'] = array('labels'=>$labels,'data'=>$order_data);
        $data['back_orders'] = array('labels'=>$labels,'data'=>$back_order_data);
        echo json_encode($data);
        
    }

    protected function year() {
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $labels = array();
        $order_data = array();
        $back_order_data = array();

        for ($i = 1; $i <= 12; $i++) {
            $labels[] = date('M', mktime(0, 0, 0, $i, 10));
            $order_data[$i] = array(
				'month' => date('M', mktime(0, 0, 0, $i, 10)),
				'total' => 0
            );
            $back_order_data[$i] = array(
				'month' => date('M', mktime(0, 0, 0, $i, 10)),
				'total' => 0
			);
        }

        $orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `orders`WHERE `user_id`='".$user_info['user_id']."' AND YEAR(date) = YEAR(NOW()) GROUP BY MONTH(date) "));
        foreach ($orders as $order) {
            //echo date('M', strtotime($order->date)); die;
            $order_data[date('n', strtotime($order->date))] = array(
				'month' => date('M', strtotime($order->date)),
				'total' => $order->total
			);
        }

        $back_orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `back_orders`WHERE `user_id`='".$user_info['user_id']."' AND YEAR(date) = YEAR(NOW()) GROUP BY MONTH(date) "));
        foreach ($back_orders as $order) {
            $back_order_data[date('n', strtotime($order->date))] = array(
				'month' => date('n', strtotime($order->date)),
				'total' => $order->total
			);
        }
        //print_r($order_data); die;
        $data['orders'] = array('labels'=>$labels,'data'=>$order_data);
        $data['back_orders'] = array('labels'=>$labels,'data'=>$back_order_data);
        echo json_encode($data);
    }

    protected function custom($years) {
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        //echo $years;
        $years = explode(',',$years);
        $order_data = [];
        foreach($years as $year) {
            for ($i = 1; $i <= 12; $i++) {
                $order_data[$year][$i] = array(
                    'month' => date('M', mktime(0, 0, 0, $i, 10)),
                    'total' => 0
                );
            }
        }
        //print_r($order_data); die;
        $labels = array();
        for ($i = 1; $i <= 12; $i++) {
            $labels[] = date('M', mktime(0, 0, 0, $i, 10));
        }
        foreach($order_data as $key=>$order) {
            $today = $key.'-'.date('m').'-'.date('d');
            $orders = DB::select(DB::raw("SELECT COUNT(*) AS total, date FROM `orders`WHERE `user_id`='".$user_info['user_id']."' AND YEAR(date) = YEAR('".$today."') GROUP BY MONTH(date) "));
            foreach ($orders as $Morder) {
                $order_data[$key][date('n', strtotime($Morder->date))] = array(
                    'month' => date('M', strtotime($Morder->date)),
                    'total' => $Morder->total
                );
            }
        }
        $data['orders'] = array('labels'=>$labels,'data'=>$order_data);
        echo json_encode($data);
    }

}

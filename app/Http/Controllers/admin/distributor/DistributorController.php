<?php

namespace App\Http\Controllers\admin\distributor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Mail;
use PDF;

class DistributorController extends Controller
{
    public function __construct(){
       
    }
    public function order_recieved_distributor(){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $order_history_data = DB::table('orders')
        ->join('User','User.seller_id','=','orders.distributor_id')
        ->join('order_status','order_status.order_status_id','=','orders.order_status_id')
        ->select('orders.*', 'order_status.*')
        ->where('User.seller_id',$user_info['seller_id'])
        ->orderBy('date', 'desc')
        ->Paginate(10);
        

        $order_history_list_tmp = json_decode(json_encode($order_history_data),true);
        $order_history_list = $order_history_list_tmp['data'];
        return view('dashboard.distributor.order_recieved_distributor',["order_history_data"=>$order_history_data,"order_history_list"=>$order_history_list]);
    }
    public function view_order_detail_distributor(Request $request){
        $order_id = $request->id;
        $inputs = $request->all();
        if(!empty($inputs['order_status_id']) && !empty($inputs['comment'])) {
            $insertData['order_id'] = $order_id;
            $insertData['order_status_id'] = $inputs['order_status_id'];
            $insertData['comment'] = $inputs['comment'];
            $insertData['date_added'] = date('Y-m-d H:i:s');
            DB::table('order_history')->insert($insertData);
            DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'order_status_id' => $inputs['order_status_id']
            ]);
        }

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        // echo "<pre>";
        // echo $order_id;
        $order_info = DB::table('orders')->where('order_id',$order_id)->get()->first();

        $order_info = json_decode(json_encode($order_info),true);

        // echo "<pre>";
        // print_r($order_info);
        // die();

        if(empty($order_info)){
            return redirect('/distributor/order_recieved_distributor');
        }

        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$order_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);

        // print_r($order_info);
        // print_r($store_info);
        // die();
        
        $distributor_info = DB::table('Seller')->where('ID',$user_info['seller_id'])->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);

        $order_products_info = DB::table('order_lines')->join('Product','Product.ID','=','order_lines.product_id')->where('order_lines.order_id',$order_id)->get();

        $order_products_info = json_decode(json_encode($order_products_info),true);

        $order_history_info = DB::table('order_history')
        ->join('order_status','order_status.order_status_id','=','order_history.order_status_id')
        ->where('order_history.order_id','=',$order_id)->get();
        $order_history_info = json_decode(json_encode($order_history_info),true);

        $order_status = DB::table('order_status')->get();
        $order_status = json_decode(json_encode($order_status),true);

        // print_r($order_info);
        // die();

        return view('dashboard.distributor.view_order_detail_distributor',['store_info'=>$store_info,'order_info'=>$order_info,'distributor_info'=>$distributor_info,'order_products_info'=>$order_products_info,'order_history'=>$order_history_info,'order_status'=>$order_status]);
    }
    public function backorder_recieved_distributor(){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $backorder_history_data = DB::table('back_orders')
        ->join('User','User.seller_id','=','back_orders.distributor_id')
        ->join('back_order_status','back_order_status.order_status_id','=','back_orders.order_status_id')
        ->select('back_orders.*', 'back_order_status.*')
        ->where('User.seller_id',$user_info['seller_id'])
        ->orderBy('back_orders.date', 'desc')
        ->Paginate(10);

        $backorder_history_list_tmp = json_decode(json_encode($backorder_history_data),true);
        $backorder_history_list = $backorder_history_list_tmp['data'];

        return view('dashboard.distributor.backorder_recieved_distributor',["backorder_history_data"=>$backorder_history_data,"backorder_history_list"=>$backorder_history_list]);
    }
    public function view_backorder_detail_distributor(Request $request){
        $backorder_id = $request->id;

        $inputs = $request->all();
        if(!empty($inputs['order_status_id']) && !empty($inputs['comment'])) {
            $insertData['order_id'] = $backorder_id;
            $insertData['order_status_id'] = $inputs['order_status_id'];
            $insertData['comment'] = $inputs['comment'];
            $insertData['date_added'] = date('Y-m-d H:i:s');
            DB::table('back_order_history')->insert($insertData);
            DB::table('back_orders')
            ->where('back_order_id', $backorder_id)
            ->update([
                'order_status_id' => $inputs['order_status_id']
            ]);
        }   

        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $backorder_info = DB::table('back_orders')->where('back_order_id',$backorder_id)->where('back_orders.distributor_id',$user_info['seller_id'])->get()->first();

        $backorder_info = json_decode(json_encode($backorder_info),true);

        if(empty($backorder_info)){
            return redirect('/distributor/backorder_recieved_distributor');
        }


        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$backorder_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);


        

        $distributor_info = DB::table('Seller')->where('ID',$user_info['seller_id'])->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);

        $backorder_products_info = DB::table('back_order_lines')->join('Product','Product.ID','=','back_order_lines.product_id')->where('back_order_lines.back_order_id',$backorder_id)->get();

        $backorder_products_info = json_decode(json_encode($backorder_products_info),true);

        $back_order_history_info = DB::table('back_order_history')
        ->join('back_order_status','back_order_status.order_status_id','=','back_order_history.order_status_id')
        ->where('back_order_history.order_id','=',$backorder_id)->get();
        $back_order_history_info = json_decode(json_encode($back_order_history_info),true);

        $back_order_status = DB::table('back_order_status')->get();
        $back_order_status = json_decode(json_encode($back_order_status),true);

        //print_r($backorder_info); die();

        return view('dashboard.distributor.view_backorder_detail_distributor',['store_info'=>$store_info,'backorder_info'=>$backorder_info,'distributor_info'=>$distributor_info,'backorder_products_info'=>$backorder_products_info,'back_order_history'=>$back_order_history_info,'back_order_status'=>$back_order_status]);
    }
    public function return_recieved_distributor(){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $return_history_data = DB::table('returns')
        ->join('User','User.seller_id','=','returns.distributor_id')
        ->select('returns.*')
        ->where('User.seller_id',$user_info['seller_id'])
        ->orderBy('date', 'desc')
        ->Paginate(10);

        $return_history_list_tmp = json_decode(json_encode($return_history_data),true);
        $return_history_list = $return_history_list_tmp['data'];


        return view('dashboard/distributor/return_recieved_distributor',["return_history_data"=>$return_history_data,"return_history_list"=>$return_history_list]);

    }
    public function view_return_detail_distributor($return_id){
        $user = auth()->user();
        $user_info = json_decode(json_encode($user),true);

        $return_info = DB::table('returns')->where('return_id',$return_id)->where('returns.distributor_id',$user_info['seller_id'])->get()->first();

        $return_info = json_decode(json_encode($return_info),true);

        if(empty($return_info)){
            return redirect('/distributor/return_recieved_distributor');
        }


        $store_info = DB::table('Buyer_Client')->join('User','User.buyer_id','=','Buyer_Client.ID')->where('User.user_id',$return_info['user_id'])->get()->first();

        $store_info = json_decode(json_encode($store_info),true);






        $distributor_info = DB::table('Seller')->where('ID',$return_info['distributor_id'])->get()->first();

        $distributor_info = json_decode(json_encode($distributor_info),true);

        $return_products_info = DB::table('return_lines')->join('Product','Product.ID','=','return_lines.product_id')->where('return_lines.return_id',$return_id)->get();

        $return_products_info = json_decode(json_encode($return_products_info),true);


        return view('dashboard.distributor.view_return_detail_distributor',['store_info'=>$store_info,'return_info'=>$return_info,'distributor_info'=>$distributor_info,'return_products_info'=>$return_products_info]);
    }
}

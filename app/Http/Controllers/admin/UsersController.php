<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the users list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $you = auth()->user();
        $users = User::all();
        return view('dashboard.admin.usersList', compact('users', 'you'));
    }

    /**
     *  Remove user
     * 
     *  @param int $id 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function remove( $id )
    {
        $user = User::find($id);
        if($user){
            $user->delete();
        }
        return redirect()->route('adminUsers');
    }

    /**
     *  Show the form for editing the user.
     * 
     *  @param int $id
     *  @return \Illuminate\Contracts\Support\Renderable
     */
    public function editForm( $id )
    {
        $user = User::find($id);
        return view('dashboard.admin.userEditForm', compact('user'));
    }

    public function edit(){

    }

    public function add_buyer_data(){
        $buyers = DB::table("Buyer_eR2m")->get()->toArray();
        //echo "<pre>";
        $count = 0;
        foreach($buyers as $result){
            $last_user_id = DB::table("User")->latest('user_id')->get()->first();
            $user_id = $last_user_id->user_id + 1;
            $user_name = str_replace(" ","",$result->Name);
            $check_user = DB::table("User")->where("user_name",'=',$user_name)->get()->first();
            if(empty($check_user)){
                $insert_buyer = array(
                    'user_id' => $user_id,
                    "supplier_id" => NULL,
                    "buyer_id" => $result->ID,
                    "seller_id" => NULL,
                    "user_type" => 0,
                    "name" => $result->Name,
                    "surname" => NULL,
                    "telephone" => $result->phone,
                    "department" => NULL,
                    "email" => $result->Email,
                    "user_name" => $user_name,
                    "password" => Hash::make("123"),
                    "status" => "Active",
                    "status_update_date" => "",
                );
                DB::table('User')->insertGetId($insert_buyer);
                $count++;
            }
        }
        echo "Total Record Inserted :". $count;
    }
    public function add_seller_data(){
        $sellers = DB::table("Seller")->get()->toArray();
        echo "<pre>";
        $count = 0;
        foreach($sellers as $result){
           // print_r($result);
            //die();
            $last_user_id = DB::table("User")->latest('user_id')->get()->first();
            $user_id = $last_user_id->user_id + 1;
            $user_name = str_replace(" ","",$result->Name);
            $user_name = str_replace("-","",$user_name);
            $check_user = DB::table("User")->where("user_name",'=',$user_name)->get()->first();
            if(empty($check_user)){
                $insert_seller = array(
                    'user_id' => $user_id,
                    "supplier_id" => NULL,
                    "buyer_id" => NULL,
                    "seller_id" => $result->ID,
                    "user_type" => 1,
                    "name" => $result->Name,
                    "surname" => NULL,
                    "telephone" => $result->Telephone,
                    "department" => NULL,
                    "email" => $result->Email,
                    "user_name" => $user_name,
                    "password" => Hash::make("123"),
                    "status" => "Active",
                    "status_update_date" => "",
                );
                //print_r($insert_seller);
               DB::table('User')->insertGetId($insert_seller);
                $count++;
            }
        }
        echo "Total Record Inserted :". $count;
    }
    public function add_supplier_data(){
        $supplier = DB::table("Supplier")->get()->toArray();
        echo "<pre>";
        $count = 0;
        foreach($supplier as $result){
        //    print_r($result);
            
            $last_user_id = DB::table("User")->latest('user_id')->get()->first();
            $user_id = $last_user_id->user_id + 1;
            $user_name = str_replace(" ","",$result->Name);
            $user_name = str_replace("-","",$user_name);
            $check_user = DB::table("User")->where("user_name",'=',$user_name)->get()->first();
            if(empty($check_user)){
                $insert_supplier = array(
                    'user_id' => $user_id,
                    "supplier_id" => $result->ID,
                    "buyer_id" => NULL,
                    "seller_id" => NULL,
                    "user_type" => 2,
                    "name" => $result->Name,
                    "surname" => NULL,
                    "telephone" => $result->phone,
                    "department" => NULL,
                    "email" => $result->email,
                    "user_name" => $user_name,
                    "password" => Hash::make("123"),
                    "status" => "Active",
                    "status_update_date" => "",
                );
                // print_r($insert_seller);
                // die();
               DB::table('User')->insertGetId($insert_supplier);
                $count++;
            }
        }
        echo "Total Record Inserted :". $count;
    }

}

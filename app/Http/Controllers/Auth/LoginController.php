<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request){
        $validatedData = $request->validate([
            'user_name' => 'required',
            'password' => 'required',
        ]);
        $userdata = array(
            'user_name' => $request->user_name,
            'password' => $request->password     
        );
        
        if (Auth::attempt($userdata)) {
            $user_info = auth::user();
            $user_info = json_decode(json_encode($user_info),true);
            
            if($user_info['user_type'] == 1){
                return redirect('distributor/dashboard');
            }else{
            //     echo "<pre>";
            //     print_r($user_info);
            // die("hello");
                return redirect('dashboard');
            }
            
        } else {        
            return redirect('login')->withErrors(['msg' =>'Invalid username or password']);;
        }
    }
    public function logout(){
        Auth::logout();
        return redirect('login');
    }
}

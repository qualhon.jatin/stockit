// eslint-disable-next-line no-unused-vars
getData('today');
var activeRange = 'today';
function changeChart() {
    getData(activeRange);
}

function getData(selectedValue) {
    activeRange = selectedValue;
    $.ajax({
        url: "chartData?range="+selectedValue,
        accepts:"application/json",
        success: function(html){
            //Chart(JSON.parse(html))
            manageData(JSON.parse(html));
        }
    });
}

function manageData(html) {
    var chart_labels = [];
    var order_data = [];
    var back_order_data = [];
    chart_labels = html.orders.labels;
    $.each(html.orders.data,function(k,v){
        order_data.push(v.total);
    });
    $.each(html.back_orders.data,function(k,v){
        back_order_data.push(v.total);
    });

    let order = {
        label: 'Orders',
        //backgroundColor: hexToRgba(getStyle('--info'), 10),
        backgroundColor: 'transparent',
        //borderColor: getStyle('--info'),
        borderColor:"#FBCE07",
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: order_data
    };

    let back_order = {
        label: 'Back Orders',
        //backgroundColor: hexToRgba(getStyle('--info'), 10),
        backgroundColor: 'transparent',
        //borderColor: getStyle('--warning'),
        borderColor:"#DD1D21",
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: back_order_data
    };

    var datasets = [];
    
    if($('#order-checkbox').prop('checked')==true) {
        datasets.push(order)
    }

    if($('#back-order-checkbox').prop('checked')==true) {
        datasets.push(back_order)
    }

    const mainChart = new Chart(document.getElementById('main-chart'), {
        type: 'line',
        data: {
            labels: chart_labels,
            datasets: datasets
        },
        options: {
        maintainAspectRatio: false,
        legend: {
            display: true
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        if (Math.floor(label) === label) {
                            return label;
                        }
    
                    },
                }
            }],
        },
        elements: {
            point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3
            }
        },
        tooltips: {
            intersect: true,
            callbacks: {
                labelColor: function(tooltipItem, chart) {
                    return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                }
            }
        }
        }
    });
}

compChange();
function compChange() {
    var years = '';
    $('.compYears').each(function(ev){
        if($(this).prop('checked')==true) {
            years += $(this).val()+',';
        }
    });
    years = years.replace(/,\s*$/, "");
    $.ajax({
        url: "chartData?range=custom&years="+years,
        accepts:"application/json",
        success: function(html){
            //Chart(JSON.parse(html))
            manageCompData(JSON.parse(html),years);
        }
    });
}

const random = () => Math.round(Math.random() * 100)
function manageCompData(html,years) {
    var custom_datasets = [];
    $.each(years.split(','),function(k,vlu){
        var tmp_data = [];
        $.each(html.orders.data[vlu],function(key,v){
            tmp_data.push(v.total);
        });
        if(k==0){
            custom_datasets.push({
                label: vlu,
                backgroundColor : '#FBCE07',
                borderColor : '#FBCE07',
                highlightFill: 'rgba(249, 177, 21)',
                highlightStroke: 'rgba(249, 177, 21)',
                data:tmp_data
            });
        } else if(k==1){
            custom_datasets.push({
                label: vlu,
                backgroundColor : '#DD1D21',
                borderColor : '#DD1D21',
                highlightFill: 'rgba(151, 187, 205)',
                highlightStroke: 'rgba(151, 187, 205)',
                data:tmp_data
            });
        } else if(k==2){
            custom_datasets.push({
                label: vlu,
                backgroundColor : '#003C88',
                borderColor : '#003C88',
                highlightFill: 'rgba(46, 184, 92)',
                highlightStroke: 'rgba(46, 184, 92)',
                data:tmp_data
            });
        } else if(k==3){
            custom_datasets.push({
                label: vlu,
                backgroundColor : '#743410',
                borderColor : '#743410',
                highlightFill: 'rgba(99, 111, 131)',
                highlightStroke: 'rgba(99, 111, 131)',
                data:tmp_data
            });
        } else if(k==4){
            custom_datasets.push({
                label: vlu,
                backgroundColor : '#008443',
                borderColor : '#008443',
                highlightFill: 'rgba(51, 153, 255)',
                highlightStroke: 'rgba(51, 153, 255)',
                data:tmp_data
            });
        }
    });
    
    var chart_labels = [];
    chart_labels = html.orders.labels;
    console.log(html.orders.data);
    new Chart(document.getElementById('canvas-1'), {
        type: 'bar',
        data: {
            labels : chart_labels,
            datasets : custom_datasets
        },
        options: {
            responsive: true
        }
    });
}
// const random = () => Math.round(Math.random() * 100)
// const barChart = new Chart(document.getElementById('canvas-1'), {
//     type: 'bar',
//     data: {
//       labels : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//       datasets : [
//         {
//           backgroundColor : 'rgba(249, 177, 21)',
//           borderColor : 'rgba(249, 177, 21)',
//           highlightFill: 'rgba(249, 177, 21)',
//           highlightStroke: 'rgba(249, 177, 21)',
//           data : [random(), random(), random(), random(), random(), random(), random()]
//         },
//         {
//           backgroundColor : 'rgba(151, 187, 205)',
//           borderColor : 'rgba(151, 187, 205)',
//           highlightFill : 'rgba(151, 187, 205)',
//           highlightStroke : 'rgba(151, 187, 205)',
//           data : [random(), random(), random(), random(), random(), random(), random()]
//         },
//         {
//           backgroundColor : 'rgba(46, 184, 92)',
//           borderColor : 'rgba(46, 184, 92)',
//           highlightFill : 'rgba(46, 184, 92)',
//           highlightStroke : 'rgba(46, 184, 92)',
//           data : [random(), random(), random(), random(), random(), random(), random()]
//         },
//         {
//           backgroundColor : 'rgba(99, 111, 131)',
//           borderColor : 'rgba(99, 111, 131)',
//           highlightFill : 'rgba(99, 111, 131)',
//           highlightStroke : 'rgba(99, 111, 131)',
//           data : [random(), random(), random(), random(), random(), random(), random()]
//         }
//       ]
//     },
//     options: {
//       responsive: true
//     }
//   })

$(document).ready(function(){

    var distributor_id = $("#distributor_id").val();
    var shell_master_categ_id = $("#shell_master_categ_id").val();
    var category_id = $("#category_id").val();
    var manufacturer_id = $("#manufacturer_id").val();
    var brand_id = $("#brand_id").val();
    var pack_size_id = $("#pack_size_id").val();

    if(distributor_id != ""){
        change_value_by_distributor(distributor_id);
    }
    setTimeout(function() {
        if(shell_master_categ_id != ""){
            change_master_categ(shell_master_categ_id,distributor_id);
        }
    }, 1000);
   
    
    setTimeout(function() {
        if(category_id != ""){
            change_categ(category_id,distributor_id);
        }
    }, 1000);
    setTimeout(function() {
        if(manufacturer_id != ""){
            change_manufacturer(manufacturer_id,category_id);
        }
    }, 2000);
    setTimeout(function() {
        if(brand_id != ""){
            change_brand(brand_id,category_id,manufacturer_id); 
        }
    }, 3000);

    setTimeout(function() {
        $("#shell_master_categ_id option[value='"+shell_master_categ_id+"']").attr("selected", "selected");
        $("#category_id option[value='"+category_id+"']").attr("selected", "selected");
        $("#manufacturer_id option[value='"+manufacturer_id+"']").attr("selected", "selected");
        $("#brand_id option[value='"+brand_id+"']").attr("selected", "selected");
        $("#pack_size_id option[value='"+pack_size_id+"']").attr("selected", "selected");
    }, 4000);

    

});
 
function change_master_categ(master_category_id,distributor_id){

    if(distributor_id == ""){
        distributor_id = $("#distributor_id").val();
    }
    if(distributor_id == ""){
        distributor_id = null;
    }
    
    if(master_category_id == "" || distributor_id == ""){
        //change_value_by_distributor("all");
    }else{
        $("#category_id").empty();
        $("#manufacturer_id").empty();
        $("#brand_id").empty();
        $("#pack_size_id").empty();
        $.ajax({
            type:"get",
            url: "change_master_categ/"+master_category_id+'/'+distributor_id,
            success: function(res){

                $("#category_id").empty();
                $("#manufacturer_id").empty();
                $("#brand_id").empty();
                $("#pack_size_id").empty();
                var categories = res.categories;
                var manufacturers = res.manufacturers;
                var brands = res.brands;
                var pack_sizes = res.pack_sizes;
               
                $("#category_id").append('<option value="">All</option>');
                $.each(categories,function(key,value){
                    $("#category_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
                });   


                $("#manufacturer_id").append('<option value="">All</option>');
                $.each(manufacturers,function(key,value){
                    $("#manufacturer_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
                });  

                $("#brand_id").append('<option value="">All</option>');
                $.each(brands,function(key,value){
                    $("#brand_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
                });  


                $("#pack_size_id").append('<option value="">All</option>');
                $.each(pack_sizes,function(key,value){
                    $("#pack_size_id").append('<option value="'+value.pack_size_id+'">'+value.pack_size_name+'</option>');
                });  

            }
        })
    }
}
function change_categ(category_id,distributor_id){
    if(distributor_id == ""){
        distributor_id = $("#distributor_id").val();
    }
    if(distributor_id == ""){
        distributor_id = null;
    }
    if(category_id == ""){
       
    }else{
        $("#manufacturer_id").empty();
        $("#brand_id").empty();
        $("#pack_size_id").empty();
        $.ajax({
            type:"get",
            url: "change_categ/"+category_id+'/'+distributor_id,
            success: function(res){
                
                $("#manufacturer_id").empty();
                $("#brand_id").empty();
                $("#pack_size_id").empty();

                var manufacturers = res.manufacturers;
                var brands = res.brands;
                var pack_sizes = res.pack_sizes;

                $("#manufacturer_id").append('<option value="">All</option>');
                $.each(manufacturers,function(key,value){
                    $("#manufacturer_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
                });  

                $("#brand_id").append('<option value="">All</option>');
                $.each(brands,function(key,value){
                    $("#brand_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
                });  


                $("#pack_size_id").append('<option value="">All</option>');
                $.each(pack_sizes,function(key,value){
                    $("#pack_size_id").append('<option value="'+value.pack_size_id+'">'+value.pack_size_name+'</option>');
                });  
            }
        });
    }
}
function change_manufacturer(manufacturer_id,category_id){
    if(category_id == ""){
        var category_id = $("#category_id").val();
    }
    if(category_id == ""){
        category_id = null
    }
    if(manufacturer_id == ""){
       
    }else{
        $("#brand_id").empty();
        $("#pack_size_id").empty();
        $.ajax({
            type:"get",
            url: "change_manufacturer/"+manufacturer_id+"/"+category_id,
            success: function(res){

                $("#brand_id").empty();
                $("#pack_size_id").empty();
                var brands = res.brands;
                var pack_sizes = res.pack_sizes;


                $("#brand_id").append('<option value="">All</option>');
                $.each(brands,function(key,value){
                    $("#brand_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
                });  


                $("#pack_size_id").append('<option value="">All</option>');
                $.each(pack_sizes,function(key,value){
                    $("#pack_size_id").append('<option value="'+value.pack_size_id+'">'+value.pack_size_name+'</option>');
                });  
            }
        });
    }
}
function change_brand(brand_id,category_id,manufacturer_id){
    if(category_id == ""){
        var category_id = $("#category_id").val();
    }
    if(manufacturer_id == ""){
        var manufacturer_id = $("#manufacturer_id").val();
    }
    if(category_id == ""){
        category_id = null
    }
    if(manufacturer_id == ""){
        manufacturer_id = null
    }
    if(brand_id == ""){
       
    }else{
        $("#pack_size_id").empty();
        $.ajax({
            type:"get",
            url: "change_brand/"+category_id+"/"+manufacturer_id+"/"+brand_id,
            success: function(res){

                $("#pack_size_id").empty();
                var pack_sizes = res.pack_sizes;

                $("#pack_size_id").append('<option value="">All</option>');
                $.each(pack_sizes,function(key,value){
                    $("#pack_size_id").append('<option value="'+value.pack_size_id+'">'+value.pack_size_name+'</option>');
                });  
            }
        });
    }
}
function submit_create_order_form(){
    var distributor_id = $("#distributor_id").val();
    if(distributor_id == ""){
        event.preventDefault();
        swal.fire("Please select a distributor.", "", "info");
        return false; 
    }else{
        $("#distributor_id_hidden").val(distributor_id);
        // this.submit();
    }
}
function submit_create_return_form(){
    
    var selected = false;

    $('input:checkbox').each(function () {
        if ($(this).is(':checked')) {
            selected = true;
        }
    });


    if(selected == false){
        event.preventDefault();
        swal.fire("Please select atleast item.", "", "info");
        return false; 
    }
}

function change_value_by_distributor(distributor_id){
    if(distributor_id == ""){
        distributor_id = "all";
    }
    $.ajax({
        type:"get",
        url: "change_distributor/"+distributor_id,
        success: function(res){

            $("#shell_master_categ_id").empty();
            $("#category_id").empty();
            $("#manufacturer_id").empty();
            $("#brand_id").empty();
            $("#pack_size_id").empty();
                
            
            var shell_master_categories = res.shell_master_categories;
            var categories = res.categories;
            var manufacturers = res.manufacturers;
            var brands = res.brands;
            var pack_sizes = res.pack_sizes;

            $("#shell_master_categ_id").append('<option value="">All</option>');
            $.each(shell_master_categories,function(key,value){
                $("#shell_master_categ_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
            });
           
            $("#category_id").append('<option value="">All</option>');
            $.each(categories,function(key,value){
                $("#category_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
            });

            $("#manufacturer_id").append('<option value="">All</option>');
            $.each(manufacturers,function(key,value){
                $("#manufacturer_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
            });

            $("#brand_id").append('<option value="">All</option>');
            $.each(brands,function(key,value){
                $("#brand_id").append('<option value="'+value.ID+'">'+value.Name+'</option>');
            });  


            $("#pack_size_id").append('<option value="">All</option>');
            $.each(pack_sizes,function(key,value){
                $("#pack_size_id").append('<option value="'+value.pack_size_id+'">'+value.pack_size_name+'</option>');
            });  
        }
    })
}

var lastSel = $("#distributor_id option:selected").val();


function change_distributor(){
    var distributor_id = $("#distributor_id").val();
    $.ajax({
        type:"get",
        url: "check_cart/",
        success: function(res){
            if(res.cart_products_count > 0){
                $("#distributor_id").val(lastSel);
                swal.fire(
                    "Do you really want to change the Distributor?", 
                    "You already have some uncompleted orders, please complete the order or remove the existing orders from order list to select another distributor.", 
                    "info");
                    
            }else{
                change_value_by_distributor(distributor_id);
            }
        }
    });
}